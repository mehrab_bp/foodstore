import { combineReducers } from 'redux';
import restaurants from "./restuarants";
import user from './user';

export default combineReducers({
    restaurants,
    user
});
