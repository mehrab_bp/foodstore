import {UPDATE_PROFILE, ADD_TO_CART} from "../constants/personActionsTypes";
const initialState = {
    profile: undefined,
    cart: {}
};


const user = (state= initialState, action)=> {
    console.log(action)
    switch (action.type){
        case UPDATE_PROFILE:
            return Object.assign({}, state,{
                profile: action.profile
            })
        case ADD_TO_CART:
            return Object.assign({}, state,{
                profile: action.profile
            })
        default:
            return state;
    }
};

export default user;
