import {UPDATE_RESTAURANTS, UPDATE_FOODPARTIES, SET_PICKED_RESTAURANT, RESET_RESTAURANTS} from "../constants/restaurantsType";
const initialState = {
    restaurants: [],
    foodParties: [],
    pickedRestaurant: {},
};


const restaurants = (state= initialState, action)=> {
    switch (action.type){
        case RESET_RESTAURANTS:
            return Object.assign({}, state,{
                restaurants: action.restaurants.filter(item => item)
            })
        case UPDATE_RESTAURANTS:
            return Object.assign({}, state,{
                restaurants: state.restaurants.concat(action.restaurants.filter(item => item))
            })
        case UPDATE_FOODPARTIES:
            return Object.assign({}, state,{
                foodParties: action.foodParties.filter(item => item)
            })
        case SET_PICKED_RESTAURANT:
            return Object.assign({}, state,{
                pickedRestaurant: action.restaurant
            })
        default:
            return state;
    }
};

export default restaurants;
