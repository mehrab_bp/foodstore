import {UPDATE_PROFILE, ADD_TO_CART} from "../constants/personActionsTypes";

class UserActions{
    updateProfile = (profile)=>{
        return{
            type: UPDATE_PROFILE,
            profile
        }
    };

    addToCart = (cart) => {
        return{
            type: ADD_TO_CART,
            cart: cart
        }
    }
    
}


var userActions = new UserActions();

export default userActions;