import {UPDATE_RESTAURANTS, UPDATE_FOODPARTIES, SET_PICKED_RESTAURANT, RESET_RESTAURANTS} from '../constants/restaurantsType';

class RestaurantsActionHandeler{
    resetRestaurants(restaurants){
        return{
            type: RESET_RESTAURANTS,
            restaurants: restaurants
        }
    }
    
    updateRestaurants(restaurants){
        return{
            type: UPDATE_RESTAURANTS,
           restaurants: restaurants
        }
    }
    
    updateFoodParties(foodParties){
        return{
            type: UPDATE_FOODPARTIES,
            foodParties: foodParties
        }
    }

    setPickedRestaurant(restaurant){
        return {
            type: SET_PICKED_RESTAURANT,
            restaurant: restaurant
        }
    }
}

var actionHandeler = new RestaurantsActionHandeler()

export default actionHandeler;