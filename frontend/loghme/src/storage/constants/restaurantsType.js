export const UPDATE_RESTAURANTS = 'UPDATE_RESTAURANTS';
export const UPDATE_FOODPARTIES = 'UPDATE_FOODPARTIES';
export const SET_PICKED_RESTAURANT = 'SET_PICKED_RESTAURANT';
export const RESET_RESTAURANTS = 'RESET_RESTAURANTS';