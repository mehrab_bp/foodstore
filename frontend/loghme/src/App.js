import React from 'react';
import MainLayout from './layouts/MainLayout';
import {Route, Switch} from 'react-router-dom';
class App extends React.Component{
  render(){
    return (
    <div className="App">
        <Switch>
                <Route path="/" 
                render={props => <MainLayout  
                        {...props} />} />
                {/* <Redirect from="/" to="/admin/login-page" /> */}
        </Switch>
    </div>
  );
}
}

export default App;
