import React, { Component } from "react";
import { Route, Switch, useLocation} from "react-router-dom";
import routes from "../routes";
import { css } from '@emotion/core';
import { ClipLoader } from 'react-spinners';
import {style} from '../variables/Variable';
import NotificationSystem from "react-notification-system";
import Footer from '../components/FooterPart/Footer';
import Header from '../components/HeaderPart/HaederMainWrapper'
import ThunkMiddleware from '../thunk/ThunkMiddleware';

function LayoutGenerator(prop){
    let location = useLocation()
    if(location.pathname === "/login" || location.pathname === "/signup"){
      return (
        <div className="wrapper" id="mainLayoutWrapper">
            <div id="main-panel" className="main-panel">
              <Switch>{prop.getRoutes(routes)}</Switch>
            </div>
        </div>
      )
    }
    else{
      return(
        <div className="wrapper" id="mainLayoutWrapper">
          <Header />
          <div id="main-panel" className="main-panel">
            <Switch>{prop.getRoutes(routes)}</Switch>
          </div>
          <Footer />
        </div>
      )
    }

}

// function PrivateRoute({prop, isAuthenticated, key}){
//     if(prop.path === "/login" || prop.path === "/signup"){
//       return (
//         <Route
//           exact path={prop.path}
//           render={props => (
//             <prop.component
//               {...props}
//             />
//           )}
//           key={key}
//         />
//       )   
//     }else{
//         if(isAuthenticated){
//           return(
//             <Route
//             exact path={prop.path}
//             render={props => (
//               <prop.component
//                 {...props}
//               />
//             )}
//             key={key}
//           />
//           )
//         }else{
// // redirect to login 
//         }
//     }
// }

class MainLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerShow: false,
      _notificationSystem: null,
      color: "black", 
      isLoginPage: false
    };
    
    this.startSpinnerShow = this.startSpinnerShow.bind(this);
    this.stopSpinnerShow = this.stopSpinnerShow.bind(this);
    this.handleNotificationClick = this.handleNotificationClick.bind(this)
  }

    componentDidMount() {
      this.setState({ _notificationSystem: this.refs.notificationSystem });
    }

  startSpinnerShow(){
      this.setState({
          spinnerShow: true
      })
  }

  stopSpinnerShow(){
    this.setState({
        spinnerShow: false
    })
  }

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if(prop.layout === "/main" ) {
        return (
          <Route
            exact path={prop.path}
            render={props => (
              <prop.component
                {...props}
              />
            )}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };

  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };

  handleNotificationClick = (position, message, color) => {
    var level;
    switch (color) {
      case "success":
        level = "success";
        break;
      case "warning":
        level = "warning";
        break;
      case "error":
        level = "error";
        break;
      case "info":
        level = "info";
        break;
      default:
        level = "info";
        break;
    }
    this.state._notificationSystem.addNotification({
      title: <i class="fa fa-exclamation-circle"></i>,
      message: (
        message === undefined ? 
        <div>{"defaultMessage"}</div>
        :
        <div id="notificationSystem" className="d-flex justify-content-center align-items-center">
            <p id="notificationMessage">
                {message}
            </p>
        </div>
      ),
      level: level,
      position: position,
      autoDismiss: 7
    });
  };

  generateSpinner(){
    return this.state.spinnerShow ? 
           ( <div className='sweet-loading'>
            <ClipLoader
                css={css}
                sizeUnit={"px"}
                size={50}
                color={'#62b0ae'}
                loading={this.state.spinnerShow}
            />
            </div>
           )
          :("")
  }

  render() {
    return (
    <div>
      <NotificationSystem ref="notificationSystem" style={style} />
        {
          this.generateSpinner()
        }
        <ThunkMiddleware 
          makeNotification={this.handleNotificationClick}
          startSpinnerShow={this.startSpinnerShow}
          stopSpinnerShow={this.stopSpinnerShow}
        >
            <LayoutGenerator getRoutes={this.getRoutes.bind(this)}/>
        </ThunkMiddleware>
    </div>
    );
  }
}

export default MainLayout;
