// import MainPage from 'views/MainPage/MainPage'
import DefaultPageWrapper from './containers/DefaultPage/DefaultPage';
import ReataurantDetail from './containers/RestaurantDetail/RestaurantDetailWrapper';
import Profile from './containers/Profile/ProfileWrapper';
import Signup from './containers/Login/Signup';
import Login from './containers/Login/Login';
import Home from './containers/Home/HomeWrapper';
const dashboardRoutes = [
  {
    path: "/default",
    valid: true,
    name: "home",
    icon: "pe-7s-graph",
    component: DefaultPageWrapper,
    layout: "/main"
  },
  {
      path: "/profiles/:path",
      valid: true,
      name: "profile",
      icon: "pe-7s-graph",
      component: Profile,
      layout: "/main"   
  },
  {
    path: "/",
    isSubPage: true,
    valid: true,
    name: "home",
    icon: "pe-7s-user",
    component: Home,
    layout: "/main"
  },
  {
    path: "/restaurant/:id",
    isSubPage: true,
    valid: true,
    name: "default",
    icon: "pe-7s-user",
    component: ReataurantDetail,
    layout: "/main"
  },
  {
    path: "/login",
    isSubPage: true,
    valid: true,
    name: "login",
    icon: "pe-7s-user",
    component: Login,
    layout: "/main",
    type: "login"
  },
  {
    path: "/signup",
    isSubPage: true,
    valid: true,
    name: "signup",
    icon: "pe-7s-user",
    component: Signup,
    layout: "/main",
    type: "login"
  },
];

export default dashboardRoutes;
