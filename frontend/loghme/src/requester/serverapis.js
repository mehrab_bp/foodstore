import axios from "axios";




const loginHeaders = {
    headers: {
        'Content-Type': 'application/json',
    }
}

// const makeLogin = (data) => {
//     return Promise.resolve(Axios.post(url + '/user-login/login', data, loginHeaders))
// }

// const makeLogout = () => {
//      return Promise.resolve(Axios.get(url + '/user-login/logout', getMethodsHeader))
//  }

// const makeVerification = (data) => {
//     return Promise.resolve(Axios.post(url + '/user-login/code-verification', data, loginHeaders))
// }

function makeQueryString(data){
    let result = ""
    let start = true
    Object.keys(data).map(key => {
        if(start){
            result += key + "=" + data[key]
            start=false
        }else{
            result += "&" + key + "=" + data[key]
        }
    })
    return result
}

class ApiRequester{
    constructor(){
        this.proxyurl = "https://cors-anywhere.herokuapp.com/";
        this.url =  "http://185.166.105.6:30030"
        this.token = localStorage.getItem("token");
    }
    

    updateToken(token){
        this.token = token
        localStorage.setItem("token", token)
    }

    getPostHeader(){
        return {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': this.token,
            }
        }
        
    }

    getGetHeader(){
        return {
            headers: {'Authorization': this.token}
        }
    }


    getFoodParties = () => {
        return Promise.resolve(axios.get(this.url + "/foodParty", this.getGetHeader()))
    }
    
    
    getUserProfile = () => {
        return Promise.resolve(axios.get(this.url + "/user", this.getGetHeader()))
    }

    getSpecRestaurant = (oid) => {
        return Promise.resolve(axios.get(this.url + `/restaurant/${oid}`, this.getGetHeader()))
    }
    addToCart = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.post(this.url + `/addToCart?${data}`, {}, this.getPostHeader()))
    }
    finalizeOrders = () => {
        return Promise.resolve(axios.post(this.url + `/finalize`, {}, this.getPostHeader()))
    }
    clearOrder = () => {
        return Promise.resolve(axios.post(this.url + `/clearCart`, {}, this.getPostHeader()))
    }
    chargeCredit = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.post(this.url + `/user/increaseCredit?${data}`, {}, this.getPostHeader()))
    }
    reduceFromCart = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.post(this.url + `/reduceFromCart?${data}`, {}, this.getPostHeader()))
    }

    makeLogin = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.post(this.url + `/login?${data}`, loginHeaders))
    }
    
    makeSignUp = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.post(this.url + `/sign-up?${data}`, loginHeaders))
    }

    getRestaurants = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.get( this.url + `/restaurants?${data}`,  this.getGetHeader()))
    }

    getRestaurantsByRestaurantName = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.get( this.url + `/restaurants/restaurantSearch?${data}`,  this.getGetHeader()))
    }

    getRestaurantsByFoodName = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.get( this.url + `/restaurants/foodSearch?${data}`,  this.getGetHeader()))
    }

    signUpWithGoogle = (data) => {
        data = makeQueryString(data)
        return Promise.resolve(axios.post(this.url + `/login/google?${data}`, loginHeaders))
    }
}

var apiClass = new ApiRequester()

export default apiClass