import React from 'react';
import starImage from "./star.svg";
import {Modal, Button} from 'react-bootstrap';

function Food(props){
    return (
        <div id="foodWrapper" class="d-flex flex-column justify-content-start align-content-center p-3">
                    <div id="imageAndFootPart" className="d-flex flex-row justify-content-end align-content-center mb-4"> 

                        <div id="descriptionPart" className="d-flex flex-column justify-content-around align-items-end">
                            <div id="title">
                                <h6>
                                   {props.data.name} 
                                </h6>
                            </div>
                            <div id="foodStarsPart" className="d-flex flex-row">
                                <span className="mr-2"><img src={starImage} alt="star"/></span>
                                <span>
                                    {Math.round(props.data.popularity * 5)}
                                </span>
                            </div>
                        </div>

                        <div className="ml-4" id="imagePart">
                            <img className="shadow-sm" src={props.data.image} alt="image"/>
                        </div>
                    
                    </div>
                    
                    <div id="pricePart" className="d-flex flex-row justify-content-between align-items-center px-5 mb-3">
                        <div id="newPrice">
                            <h6 className="d-flex flex-row">
                                {
                                    !props.data.oldPrice ? <span>تومان</span> :""
                                }

                                <span>{props.data.price}</span>
                            </h6>
                        </div>
                        {   
                            props.data.oldPrice ?
                            <div id="oldPrice">
                                <h6>
                                    {props.data.oldPrice}
                                </h6>
                            </div>
                            :""    
                        }
                    </div>

                    <div id="footerDescription" className="w-100 d-flex flex-row justify-content-center align-content-center pt-2">
                        <div id="existancePart" className="w-100 d-flex flex-row justify-content-between align-items-center">
                            <div id="buyPart" className="px-3 d-flex align-items-center">
                                <h6 className="m-0 btn" onClick={() => props.addToCart(props.data)}>افزودن به سبد خرید</h6>
                            </div>
                            {
                                props.data.count ?
                                <div id="existOrNotPart" className="px-3 d-flex align-items-center">
                                    <h6 className="m-0">
                                        موجودی :   {props.data.count}
                                    </h6>
                                </div>
                                :""
                            }
                        </div>
                    </div>
            </div>
    )
}

function FoodModal(props) {
    return (
      <Modal
        show={props.show}
        onHide={props.handleShow}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        // centered
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body className='modal-wrapper'>
            <Food data={props.data} addToCart={props.addToCart}/>
        </Modal.Body>
      </Modal>
    );
}
  

export default FoodModal;