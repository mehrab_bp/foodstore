import React from 'react';
import {Modal, Table} from 'react-bootstrap';


function Order(props){
    return (
        <Table responsive striped className="orderWrapper rounded">
        <thead>
            <tr>
            <th>قیمت</th>
            <th>تعداد</th>
            <th>نام غذا</th>
            <th>ردیف</th>
            </tr>
        </thead>
        <tbody>
            {props.orders.map((item, index) => (
                <tr>
                    <td>
                        {item.price}
                    </td>
                    <td>
                        {item.number}
                    </td>
                    <td>
                        {item.foodName}
                    </td>
                    <td>
                        {index + 1}
                    </td>
                </tr>
            ))}
        </tbody>
        </Table>
    )
}


function OrderModal(props) {
    let sum = 0
    if(props.data && Array.isArray(props.data.orders)){
        props.data.orders.map(item => {
            sum += item.price * item.number
        })
    }

    return (
      <Modal
        show={props.show}
        onHide={props.handleShow}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        // centered
      >
        <Modal.Header closeButton className="modeal-header">
            <Modal.Title>
                {props.data.restaurantName}
            </Modal.Title>
        </Modal.Header>
        <Modal.Body className='modal-wrapper'>
            {
                Array.isArray(props.data.orders) ?
                <Order orders={props.data.orders}/>
                :""
            }
        </Modal.Body>
        <Modal.Footer className="d-flex flex-row-reverse">
            <span> : جمع کل  </span>
            <span>{sum}</span>
            <span>تومان </span>
        </Modal.Footer>
      </Modal>
    );
}
  

export default OrderModal;