import React from "react";

export default function Footer(){
    return(
        <footer class="row">
            <div id="copyRightPartWrapper" class="col-12">
                <div class="d-flex py-2 flex-row justify-content-center align-items-center">
                    <h6>تمامی حقوق متعلق به لقمه است</h6>
                    <span class="ml-1">&copy;    </span>         
                </div>
            </div>
        </footer>
    )
}