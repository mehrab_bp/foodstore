import React from 'react'
import logo from '../../assets/images/LOGO.png';
import {Link, withRouter} from 'react-router-dom';
import thunk from "../../thunk/ThunkApiMainHandeler";
class HeaderWrapper extends React.Component{
    makeLogout = (e) => {
        thunk.thunkMakeLogout({}, this.props.history, true)
    }
    

    render(){
        return (
            <header class="row w-100 bottomLightShadow" id="headerPart">
            <div class="container">
                <div id="headerContentWrapper" class="w-100 py-2 d-flex flex-row justify-content-between align-items-center">        
                    <div id="exitAndCartPart" class="d-flex flex-row justify-content-center align-items-center">
                        <div id="exitButtonPart" class="mr-3">
                            <a className="btn" onClick={this.makeLogout} href="">خروج</a>
                        </div>
                        <div id="routeLocationButtonPart" class="mr-3">
                            <Link to="/profiles/orders">حساب کاربری</Link>
                        </div>
                        <div id="cartPart">
                            <div class="cart-logo-container">
                                <span class="cart-logo-count">3</span>
                                <i class="flaticon-smart-cart"></i>
                            </div>
                        </div>
                    </div>

                    <div id="logoPart" class="rounded-circle">
                        <Link to="/">
                            <img src={logo} alt="main-logo"/>
                        </Link>
                    </div>
                
                    </div>
                </div>   
            </header>
        )
    }
}

export default withRouter(HeaderWrapper);