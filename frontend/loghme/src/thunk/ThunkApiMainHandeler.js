import APIRequester from '../requester/serverapis';
// import {setUsers, setMissions, setCars, setSimcards, setOrigins, setDevices, setDeviceCompleteInfo, setMessages, setUsersTree} from './dispatcherActions';
import restaurantActions from '../storage/actions/restaurantActions';
import userActions from '../storage/actions/userActions';

function refreshPage(){
    document.location.reload(true)
}

class ThunkMiddleware{
    constructor(){
        this.makeNotification={}
        this.startSpinnerShow={}
        this.stopSpinnerShowInner={}
        this.history = {}
    }

    setHistory(history){
        this.history = history;
    }

    setNotificationProps(props){
        this.makeNotification = props.makeNotification
        this.startSpinnerShow = props.startSpinnerShow
        this.stopSpinnerShowInner = props.stopSpinnerShow        
    }

    stopSpinnerShow(){
        setTimeout(() => this.stopSpinnerShowInner(), 500)
    }

    handleError(err){
        this.stopSpinnerShowInner()
        console.log(err.response)
        if(err.response && err.response.status===401){
            this.history.push('/login')
            this.makeNotification("tc", "لطفا در ابتدا وارد سامانه شوید", "error")
        }else if(err.response){
            this.makeNotification("tc", err.response.data, "error")
        }else{
            this.makeNotification("tc", "مشکلی پیش امده است. لطفا دوباره  تلاش کنید ", "error")
        }
    }

    showSuccessMessage(message){
        if(message){
            this.makeNotification("tc", message, "error")
        }else{
            this.makeNotification("tc", "! با موفقیت انجام شد", "success")
        }
    }

    thunkGetFoodParties = (spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.getFoodParties().then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            dispatch(restaurantActions.updateFoodParties(res.data))
        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                // this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkGetUserProfile = (userQuery, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.getUserProfile(userQuery).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            dispatch(userActions.updateProfile(res.data))
        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkGetSpecRestaurant = (oid, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.getSpecRestaurant(oid).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            dispatch(restaurantActions.setPickedRestaurant(res.data))
        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })   
    }

    thunkAddToCart = (data, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.addToCart(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            dispatch(this.thunkGetUserProfile("", spinnerFunctions))
        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })   
    }

    thunkFinalizeCart = (spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.finalizeOrders().then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            dispatch(this.thunkGetUserProfile("", spinnerFunctions))
        }).catch(err => {
            console.log(err.response)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })  
    }

    thunkClearCart = (spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.clearOrder().then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            dispatch(this.thunkGetUserProfile("", spinnerFunctions))
        }).catch(err => {
            console.log(err.response)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkChargeCredit = (data, spinnerFunctions) => dispatch => {
        if(parseInt(data.amount)){
            if(spinnerFunctions){
                this.startSpinnerShow()
            }
            APIRequester.chargeCredit(data).then((res) => {
                if(spinnerFunctions){
                    this.stopSpinnerShow()
                }
                this.showSuccessMessage()
                dispatch(this.thunkGetUserProfile("", spinnerFunctions))
            }).catch(err => {
                console.log(err.response)
                if(spinnerFunctions){
                    this.handleError(err)
                    // refreshPage()
                }
            })
        }else{
            this.makeNotification("tc", "مقدار وارد شده صحیح نیست", "info")
        }
    }

    thunkReduceFromCart = (data, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.reduceFromCart(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            dispatch(this.thunkGetUserProfile("", spinnerFunctions))
        }).catch(err => {
            console.log(err.response)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkMakeLogin = (data, history, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.makeLogin(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            APIRequester.updateToken(res.data)
            dispatch(this.thunkGetUserProfile("", spinnerFunctions))
            history.push('/');
        }).catch(err => {
            console.log(err.response)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkMakeSignUp = (data, history, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.makeSignUp(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            history.push('/login');
        }).catch(err => {
            console.log(err.response)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkMakeLogout = (data, history, spinnerFunctions) =>  {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.updateToken("");
        history.push("/login");
    }

    thunkGetRestaurants = (data, spinnerFunctions, shouldRefreshState) => dispatch => {
        spinnerFunctions=true
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.getRestaurants(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }

            if(shouldRefreshState){
                dispatch(restaurantActions.resetRestaurants(res.data))
            }else{
                dispatch(restaurantActions.updateRestaurants(res.data))
            }

        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkGetRestaurantsByRestaurantName = (data, spinnerFunctions, shouldRefreshState) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.getRestaurantsByRestaurantName(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            
            
            if(shouldRefreshState){
                dispatch(restaurantActions.resetRestaurants(res.data))
            }else{
                dispatch(restaurantActions.updateRestaurants(res.data))
            }
        
        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkGetRestaurantsByFoodName = (data, spinnerFunctions, shouldRefreshState) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.getRestaurantsByFoodName(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            
            if(shouldRefreshState){
                dispatch(restaurantActions.resetRestaurants(res.data))
            }else{
                dispatch(restaurantActions.updateRestaurants(res.data))
            }

        }).catch(err => {
            console.log(err)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

    thunkGoogleSignUp = (data, spinnerFunctions) => dispatch => {
        if(spinnerFunctions){
            this.startSpinnerShow()
        }
        APIRequester.signUpWithGoogle(data).then((res) => {
            if(spinnerFunctions){
                this.stopSpinnerShow()
            }
            this.showSuccessMessage()
            APIRequester.updateToken(res.data)
            console.log(res.data)
            dispatch(this.thunkGetUserProfile("", spinnerFunctions))
            this.history.push('/');
        }).catch(err => {
            console.log(err.response)
            if(spinnerFunctions){
                this.handleError(err)
                // refreshPage()
            }
        })
    }

}

var thunk = new ThunkMiddleware();

export default thunk;