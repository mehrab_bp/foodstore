import React, {Fragment} from 'react';
import ThunkApiHandeler from './ThunkApiMainHandeler';
import {withRouter} from 'react-router-dom'

class ThunkMiddleware extends React.Component{
    constructor(props){
        super(props)
        ThunkApiHandeler.setNotificationProps(this.props)
        ThunkApiHandeler.setHistory(this.props.history)
    }
    render(){
    return (
        <Fragment>
            {this.props.children}
        </Fragment>
    );
}
}

export default withRouter(ThunkMiddleware);
