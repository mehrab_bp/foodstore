import React, { Fragment } from "react"
import OrderModal from '../../../components/OrderModal/OrderModal';

function Status({data, selectOrderToShowDetails}){
    if(data.status === "done"){
        return(
                <h6 className="btn inResult htagPadding" onClick={() => selectOrderToShowDetails(data)}>مشاهده فاکتور</h6>
        )
    }else if(data.status === "findingDelivery"){
        return(
                <h6 className="inSearching htagPadding">در جستحوی پیک</h6>
        )
    }else {
        return(
                <h6 className="inProcess htagPadding">پیک در مسیر</h6>
        )
    }
}

function Order({data, index, selectOrderToShowDetails}){
    return(
        <div className="row mx-1 my-3">
            <div id="ordersWrapper" className="col-12">

                <div id="orderDetailsWrapper" className="row flex-row-reverse align-items-center">
                    <div id="orderNumber" className="py-2 col-1 d-flex justify-content-center align-items-center">
                        {index + 1}
                    </div>

                    <div id="restaurantName" className="py-2 col-6 d-flex justify-content-center align-items-center">
                        {data.restaurantName}
                    </div>

                    <div id="orderStatus" className="py-2 col-5 d-flex justify-content-center align-items-center">
                          <Status data={data} selectOrderToShowDetails={selectOrderToShowDetails}/>  
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default class OrdersWrapper extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            showOrderModal: false,
            selectedOrder: {}
        }
    }

    handleShow = () => {
        this.setState({
            showOrderModal: !this.state.showOrderModal
        })
    }

    selectOrderToShowDetails = (data) => {
        this.setState({
            selectedOrder: data  
        })
        this.handleShow()
    }



    render(){
        return(
            <Fragment>                    
                {
                    this.props.profile.finalizedOrders.map((item, index) => (
                        <Order index={index} data={item} selectOrderToShowDetails={this.selectOrderToShowDetails}/>
                    ))
                }
                <OrderModal 
                    handleShow={this.handleShow}
                    data={this.state.selectedOrder}
                    show={this.state.showOrderModal}
                />
            </Fragment>
        )
    }
}