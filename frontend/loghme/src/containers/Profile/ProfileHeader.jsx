import React from "react";


export default function ProfileHeader({profile}){
    return(
                <div className="row w-100">
                    <section id="mainUserInfoWrapper" className="col-12">
                        <div  className="container">
                            <div className="row flex-row-reverse w-100 py-3">
                            
                                <div className="px-0 col-lg-3 col-md-4 h-100">
                                    <div id="namePart" className="h-100 d-flex flex-row-reverse justify-content-start align-items-center">
                                        <div id="logoWrapper" className="ml-1">
                                            <i className="flaticon-account"></i>
                                        </div>
                                        <div id="nameWrapper">
                                            <h3 className="fontBold">
                                                {profile.name}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
        
        
                                <div className="col-lg-6 col-md-4"></div>
                                
        
                                <div className="col-lg-3 col-md-4 px-0">
                                    <div id="moreInfoWrapper" className="d-flex flex-column">
                                        
                                        <div id="phonePart" className="d-flex flex-row-reverse justify-content-start">
                                            <span className="ml-3">
                                                <i className="flaticon-phone"></i>
                                            </span>
                                            <h4>
                                                {profile.phoneNumber}
                                            </h4>
                                        </div>
                                        
                                        <div id="emailPart" className="d-flex flex-row-reverse justify-content-start">
                                            <span  className="ml-3">
                                                <i className="flaticon-mail"></i>
                                            </span>
                                            <h4>
                                                {profile.email}
                                            </h4>
                                        </div>
        
                                        <div id="walletPart" className="d-flex flex-row-reverse justify-content-start">
                                            <span className="ml-3">
                                                <i className="flaticon-card"></i>
                                            </span>
                                            <h4>
                                                {profile.credit}
                                            </h4>
                                        </div>
                                    
                                    </div>
                                </div>
        
                            </div>
                        </div>
                    </section>
                </div>  
    );
}