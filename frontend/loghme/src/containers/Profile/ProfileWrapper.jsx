import React from "react";
import Header from './ProfileHeader';
import {useParams,useLocation, Switch, Route} from "react-router-dom";
import Orders from "./Orders/OrdersWrapper";
import Credits from "./Credits/CreditWrapper";
import thunk from '../../thunk/ThunkApiMainHandeler';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PannelTools from './PanelButtons';

function Routes({profile, chargeCredit}){
    let params = useParams()
    let locations = useLocation()
    let basePath = locations.pathname.substring(0, locations.pathname.indexOf(params.path) - 1)
    return(
        <Switch>
            <Route exact path={basePath + "/credit"} 
            component={() => <Credits chargeCredit={chargeCredit}/>} />

            <Route 
            // path={basePath + "/orders"}
            component={() => <Orders profile={profile}/>} />
        </Switch>
    )
}

class ProfileWrapper extends React.Component{
    constructor(props){
        super(props)
        
        this.chargeCredit = this.chargeCredit.bind(this);
    }
    componentDidMount(){
        if(this.props.profile === undefined){
            this.props.setProfileToDB("?id=1")
        }
    }

    chargeCredit(data){
        data.userId = this.props.profile.id
        this.props.chargeCredut(data)
    }

    render(){
        return(
            <article className="w-100 row" id="article">
                <div className="container-fluid p-0 m-0">
                    {
                        this.props.profile ? <Header profile={this.props.profile} />: ""
                    }
                    <PannelTools>
                        {
                            this.props.profile ? <Routes  profile={this.props.profile} chargeCredit={this.chargeCredit}/>: ""
                        }
                    </PannelTools>
                </div>
            </article>
        )
    }
}

const mapStateToProps = state => {
	return {
		profile: state.user.profile,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		setProfileToDB: (queryString) => {
			dispatch(thunk.thunkGetUserProfile(queryString, true));
        },
        chargeCredut: (queryString) => {
            dispatch(thunk.thunkChargeCredit(queryString, true));
        }
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfileWrapper));