import React from 'react'
import {useParams, Link} from "react-router-dom";

export default function PannelTools(props){    
        let path = useParams().path
        return(
            <div className="row w-100">
                    <section className="col-12" id="mainContentWrapper">
                        <div className="container pb-3" id="orderAndWalletDetailsComponent">
                            <div className="row mx-5 justify-content-center">
                                <div className="col-lg-9 col-md-10">
                                    <div className="row flex-row-reverse lightWrapperShadow"  id="titlePart">
                                        <div className={`col-sm-6 btn py-3 d-flex justify-content-center align-items-center ${path!=="orders" ? "activeColor":"inactiveBackground"}`}>
                                            <h4>
                                                <Link to={'/profiles/orders'}>
                                                     سفارش ها
                                                </Link>
                                            </h4>
                                        </div>
                
                                        <div className={`col-sm-6 btn py-3 d-flex justify-content-center align-items-center ${path==="orders" ? "activeColor":"inactiveBackground"}`}>
                                            <h4>
                                                <Link to={'/profiles/credit'}>
                                                        افزایش اعتبار       
                                                </Link>
                                            </h4>
                                        </div>  
                                    </div>
                                </div>
                            
                            </div>
                            
                            {
                                props.children
                            }
        
                        </div>
                    </section>
            </div>
        )
}
