import React from "react"

export default class OrdersWrapper extends React.Component{
    constructor(props){
        super(props)
        
        this.state = {
            credit: 0
        }
        this.handleInput = this.handleInput.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }    

    handleInput(event){
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    addToCart(){
        let data = {
            amount: this.state.credit,
        }
        this.props.chargeCredit(data)
    }

    render(){
        return(
            <div className="row my-4 mx-5 py-3 justify-content-center">
                <div className="col-lg-9 col-md-10">
                    <div id="chargeCreditPartWrapper" className="row flex-row-reverse justify-content-between">
                        <div className="col-8 form-group">
                            <input className="form-control inputStyle" type="text" name="credit"
                            onChange={this.handleInput} 
                            placeholder="میزان افزایش اعتبار"/>
                        </div>
                        <div className="col-3">
                            <button className="btn mediumTorquqizBackground buttonStyle" onClick={this.addToCart}>افزایش</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}