import React from 'react'
import Logo from './images/LOGO.png';

class HomeHeader extends React.Component{
    

    render(){
        return (
            <div id="ContentheaderPart" className="d-flex flex-column justify-content-center align-items-center">
                <div id="loghmeImage">
                    <img src={Logo} alt="logo"/>
                </div>
                <div id="content">
                    <p className="text-center">اولین و بزرگترین وبسایت سفارش غذا در دانشگاه تهران</p>
                </div>
            </div>
        )  
    }
}


export default HomeHeader 