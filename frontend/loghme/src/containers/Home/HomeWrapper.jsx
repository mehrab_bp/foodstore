import React from 'react'
import Header from "./HomeHeader";
import Content from "./Content/ContentWrapper";
class HomeWrapper extends React.Component{
    

    render(){
        return (
            <div id="homePage">
                <Header />
                <Content />
            </div>
        )  
    }
}


export default HomeWrapper 