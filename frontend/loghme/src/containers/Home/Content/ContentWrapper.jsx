import React from 'react'
import SearchBox from './SearchBox';
import FoodParty from './FoodParty/FoodParty';
import Restaurants from './Restaurants/RestaurantsListWrapper';
import thunk from "../../../thunk/ThunkApiMainHandeler";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

const restaurantsRetreivingStates = {
    default: "default",
    byRestaurantName: "byRestaurantName",
    byFoodName: "byFoodName"
}

class ContentWrapper extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            pageNumber: 1,
            pageSize: 10,
            searchPhrase: null,
            retreivingState: restaurantsRetreivingStates.default
        }
    }


    setDeafualtValueToState(){
        this.setState({
            pageNumber: 1,
            pageSize: 10,
            searchPhrase: null,
            retreivingState: restaurantsRetreivingStates.default
        })
    }

    componentDidMount(){
        if(this.props.restaurants.length === 0 ){
            let data = {
                pageNumber: this.state.pageNumber,
                pageSize: this.state.pageSize
            }
            this.setDeafualtValueToState()

            this.props.setRestaurantsToDB(data, true)
        }
    }

    getRestaurantsBySearch = (retreivingState, searchPhrase) => {
        this.setState({
            retreivingState: retreivingState,
            searchPhrase: searchPhrase,
            pageNumber: 1
        })
        
        let data = {
            pageNumber: 1,
            pageSize: this.state.pageSize,
            search: searchPhrase
        }

        if(searchPhrase){
            if(retreivingState === restaurantsRetreivingStates.byRestaurantName){
                this.props.setRestaurantsByRestaurantNameToDB(data, true)
            }else{
                this.props.setRestaurantsByFoodNameToDB(data, true)
            }
        }else{  //get default restaurants
            this.props.setRestaurantsToDB(data, true)
        }
    }

    getNextPage = () => {
        let pageNumber = this.state.pageNumber + 1
        this.setState({
            pageNumber: pageNumber
        })
        let data = {
            pageNumber: pageNumber,
            pageSize: this.state.pageSize
        }
        if(this.state.retreivingState === restaurantsRetreivingStates.default){
            this.props.setRestaurantsToDB(data, false)
        }else if(this.state.retreivingState === restaurantsRetreivingStates.byRestaurantName){
            data.search = this.state.searchPhrase
            this.props.setRestaurantsByRestaurantNameToDB(data, false)
        }else{ //by food name
            data.search = this.state.searchPhrase
            this.props.setRestaurantsByFoodNameToDB(data, false)
        }
    }

    render(){
        return (
            <div id="contentWrapper" className="container-fluid">
                <SearchBox restaurantsRetreivingStates={restaurantsRetreivingStates} getRestaurantsBySearch={this.getRestaurantsBySearch}/>
                {this.state.retreivingState === restaurantsRetreivingStates.default ? <FoodParty />:""}
                <Restaurants restaurants={this.props.restaurants}/>
                <div className="d-flex justify-content-center align-items-center mb-5 pb-5">
                    <button className="btn btn-info px-5 py-3" onClick={this.getNextPage}>نتایج بیشتر</button>
                </div>
            </div>
        )  
    }
}

const mapStateToProps = state => {
	return {
		restaurants: state.restaurants.restaurants,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		setRestaurantsToDB: (data, refresh) => {
			dispatch(thunk.thunkGetRestaurants(data, true, refresh));
        },
        setRestaurantsByRestaurantNameToDB: (data, refresh) => {
            dispatch(thunk.thunkGetRestaurantsByRestaurantName(data, true, refresh))
        },
        setRestaurantsByFoodNameToDB: (data, refresh) => {
            dispatch(thunk.thunkGetRestaurantsByFoodName(data, true, refresh))
        },
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentWrapper));