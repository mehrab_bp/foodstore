import React from 'react'
import Restaurant from './RestaurantDetail'



export default class RestaurantsListWrapper extends React.Component{
    render(){
        return (
            <div id="restaurantsListWrapper" className="row mt-5 mb-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div id="restaurantsListWrapperHeader" className="d-flex justify-content-center align-items-center px-4">
                            <h2 className="text-center">
                                رستوران ها
                            </h2>
                        </div>
                    </div>

                    <div className="row flex-wrap mt-3">
                        
                        {
                            this.props.restaurants.map(item => (
                                <div className="col-lg-3 col-md-4">
                                    <Restaurant restaurant={item}/>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        )  
    }
}