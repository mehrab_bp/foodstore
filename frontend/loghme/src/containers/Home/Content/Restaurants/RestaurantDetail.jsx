import React from 'react'
import {Link} from 'react-router-dom';

class RestaurantDetail extends React.Component{
    render(){
        return (
            <div id="restaurantDetail" className="d-flex flex-column justify-content-center align-items-center shadow rounded py-2 mt-2 mb-4">
                <div id="resaurantLogo" className="shadow-sm mt-3">
                    <img src={this.props.restaurant.logo} alt="restaurantLogo"/>
                </div>
                <div id="restaurantName" className="mt-3">
                    <h6>{this.props.restaurant.name}</h6>
                </div>
                <div id="refToRestaurant" className="mt-3">
                    <h6 className="px-3 py-1"><Link to={`/restaurant/${this.props.restaurant.id}`}>نمایش منو</Link></h6>
                </div>
            </div>
        )  
    }
}



export default RestaurantDetail;