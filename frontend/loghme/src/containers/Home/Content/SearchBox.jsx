import React from 'react'

class SearchBox extends React.Component{
    constructor(props){
        super(props)
        
        this.state={
            status: null,
            value: null
        }
    }    

    setInput = (e) => {
        this.setState({
            status: e.target.name,
            value: e.target.value
        })
    }

    findByStatus = (e) => {
        if(this.state.value && this.state.value !== ""){
            this.props.getRestaurantsBySearch(this.state.status, this.state.value)
        }else{
            this.props.getRestaurantsBySearch(this.props.restaurantsRetreivingStates.default, null)
        }
    }

    render(){
        return (
            <div className="row flex-row justify-content-center">
                <div id="searchBoxWrapper" className="col-lg-4 col-md-9">
                    <div id="searchBox" className="shadow row">
                        <div className="col-md-4 activeSearchBox">
                                <h6 className="btn" onClick={this.findByStatus}>جستجو</h6>
                        </div>
                        <div className="col-md-4 ordinarySearchBox">
                                <input onChange={this.setInput} name={this.props.restaurantsRetreivingStates.byRestaurantName} placeholder={'نام رستوران'}/>
                        </div>
                        <div className="col-md-4 ordinarySearchBox">
                                <input onChange={this.setInput} name={this.props.restaurantsRetreivingStates.byFoodName} placeholder={'نام غذا'}/>
                        </div>  
                    </div>
                </div>
            </div>
        )  
    }
}


export default SearchBox 