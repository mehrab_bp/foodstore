import React from 'react'

class CountDown extends React.Component{
    constructor(props){
        super(props)
        
        this.state = {
            seconds: this.props.second,
            minutes: this.props.minutes
        }
    }

    componentDidMount() {
        this.myInterval = setInterval(() => {
            const { seconds, minutes } = this.state
            if (seconds > 0) {
              this.setState(({ seconds }) => ({
                seconds: seconds - 1
              }))
            }
            if (seconds === 0) {
              if (minutes === 0) {
                clearInterval(this.myInterval)
              } else {
                this.setState(({ minutes }) => ({
                  minutes: minutes - 1,
                  seconds: 59
                }))
              }
            }
        }, 1000)
    }

    render(){
        const { minutes, seconds } = this.state

        return (
            <div id="countDownComponent" className="d-flex justify-content-center align-items-center">
                <h6 className="d-flex justify-content-center align-items-center px-4 py-2">
                    <span>{ minutes }:{ seconds < 10 ? `0${ seconds }`:seconds }</span>
                    <span className="ml-2">{this.props.title}</span>
                </h6>  
            </div>
        )  
    }
}


export default CountDown 