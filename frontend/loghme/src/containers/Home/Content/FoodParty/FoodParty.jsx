import React from 'react'
import Countdown from './CountDown';
import Carousel from './SlickCarousel';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import thunk from '../../../../thunk/ThunkApiMainHandeler';
import FoodModal from '../../../../components/FoodModal/FoodModal';
// foodParties

class FoodPartyWrapper extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            foodParties: [],
            showFoodModal: false,
            selectedFood: {}
        }
    }
    
    generateFoodPartyData(buldData){
        let result = []
        if(Array.isArray(buldData)){
            buldData.map(restaurant => {

                if(Array.isArray(restaurant.menu)){
                    restaurant.menu.map(food => {
                        let data = food
                        data.restaurant = restaurant.name
                        data.restaurantId = restaurant.id
                        result.push(data)
                    })
                }

            })
        }
        console.log(result)
        this.setState({
            foodParties: result
        })
    }

    componentDidMount(){
        if(this.props.foodParties.length === 0 ){
            this.props.setFoodPartiesToDB()
        }else{
            this.generateFoodPartyData(this.props.foodParties)
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.foodParties !== this.props.foodParties){
            this.generateFoodPartyData(this.props.foodParties)
        }
    }    

    handleShow = () => {
        this.setState({
            showFoodModal: !this.state.showFoodModal
        })
    }

    selectFood = (data) => {
        this.setState({
            selectedFood: data  
        })
        this.handleShow()
    }

    addToCart = (data) => {
        this.handleShow()
        let finalData = {
            food_id: data.id,
            restaurant_id: data.restaurantId
        }
        this.props.addToCart(finalData)
    }
  
    render(){
        return (
            <div id="foodPartyWrapper" className="row">
                <div className="container-fluid">
                    <div id="foodPartyheaderPart" className="row w-100">
                        <div className="w-100 d-flex flex-column justify-content-start align-items-center">
                            <div id="title" className="px-4 mt-4 mb-3">
                                <h2> ! جشن غذا</h2>
                            </div>
                            <div id="timer">
                                <Countdown second={30}
                                minutes={2}
                                title={' : زمان باقی مانده'}
                                />
                            </div>
                        </div>
                    </div>
                    <div id="carouselPart" className="">
                        {
                            this.state.foodParties.length > 0 ? 
                            <Carousel foodParties={this.state.foodParties} selectFood={this.selectFood}/>:""
                        }
                        
                    </div>
                </div>
                <FoodModal 
                    handleShow={this.handleShow}
                    data={this.state.selectedFood}
                    show={this.state.showFoodModal}
                    addToCart={this.addToCart}
                />
            </div>
        )  
    }
}

  
const mapStateToProps = state => {
	return {
		foodParties: state.restaurants.foodParties,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		setFoodPartiesToDB: () => {
			dispatch(thunk.thunkGetFoodParties(true));
        },
        addToCart: (data) => {
            dispatch(thunk.thunkAddToCart(data, true))
        }
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FoodPartyWrapper));
