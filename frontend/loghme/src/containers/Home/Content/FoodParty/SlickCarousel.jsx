import React from "react";
import Slider from "react-slick";
import Food from './FoodWrapper';

export default class SlcikCarousel extends React.Component {
    render() {
      var settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: this.props.foodParties.length > 6 ? 3:2,
        slidesToScroll: 1,
        autoplay: true,
        pauseOnHover: true,
        // focusOnSelect: true,
        className: "shadow slickCarousel",
        
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 800,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              initialSlide: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };
      return (
        <Slider {...settings}>
            {
              this.props.foodParties.map(item => (
                <Food data={item} selectFood={this.props.selectFood}/>
              ))
            }
        </Slider>
      );
    }
  }