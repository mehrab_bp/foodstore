import React from "react"
import starImage from "../../images/star.svg"

export default class FoodContent extends React.Component{
    

    render(){
        return(
            <div id="foodWrapper" class="d-flex flex-column justify-content-start align-content-center shadow mx-4 my-4 p-3">
                    <div id="imageAndFootPart" className="d-flex flex-row justify-content-end align-content-center mb-4"> 

                        <div id="descriptionPart" className="d-flex flex-column justify-content-around align-items-end">
                            <div id="title">
                                <h6>
                                   {this.props.data.name} 
                                </h6>
                            </div>
                            <div id="foodStarsPart" className="d-flex flex-row">
                                <span className="mr-2"><img src={starImage} alt="star"/></span>
                                <span>
                                    {Math.round(this.props.data.popularity * 5)}
                                </span>
                            </div>
                        </div>

                        <div className="ml-4" id="imagePart">
                            <img className="shadow-sm" src={this.props.data.image} alt="image"/>
                        </div>
                    
                    </div>
                    
                    <div id="pricePart" className="d-flex flex-row-reverse justify-content-between align-items-center px-5 mb-3">
                        <div id="oldPrice">
                            <h6>
                                {this.props.data.oldPrice}
                            </h6>
                        </div>
                        <div id="newPrice">
                            <h6>
                                {this.props.data.price}
                            </h6>
                        </div>
                    </div>

                    <div id="existancePart" className="d-flex flex-row-reverse justify-content-between align-content-center px-4 mb-3">
                        <div id="existOrNotPart" className="px-3 d-flex align-items-center">
                            <h6 className="m-0 d-flex align-items-center">
                                {this.props.data.count > 0 ? 
                                "موجود : " + this.props.data.count : "ناموحود"
                                }
                            </h6>
                        </div>
                        <div id="buyPart" className="px-3 d-flex align-items-center">
                            <h6 className="m-0 btn" onClick={() => this.props.selectFood(this.props.data)}>خرید</h6>
                        </div>
                    </div>

                    <div id="footerDescription" className="d-flex flex-row justify-content-center align-content-center">
                        <div id="restaurantName" className="px-3 py-1 d-flex align-items-center">
                            <h5  className="mt-2 mb-1">
                                {this.props.data.restaurant}
                            </h5>
                        </div>
                    </div>

            </div>

        )
    }
}
