import React from "react"
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import thunk from "../../thunk/ThunkApiMainHandeler";
import { GoogleLogin } from 'react-google-login';


class LoginPage extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            email: null, 
            password: null
        }
    }

    setInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    submitForm = (e) => {
        e.preventDefault();
        let data = {
            email: this.state.email,
            password: this.state.password
        }
        this.props.makeLogin(data, this.props.history)
    }

    responseGoogle = (response) => {
        let data = {
            idToken: response.tokenId
        }
        this.props.makeGoogleSignup(data)
    }

    render(){
    return(
        <div id="loginWrapper" class="container-fluid d-flex flex-column justify-content-center align-items-center">
            <div id="main" class="row d-flex flex-column justify-content-start align-items-center"> 
                <p id="sign" > ! ورود به لقمه</p>
                <form id="form1">
                <input id="username" name={'email'} type="text"  placeholder="کد کاربری" onChange={this.setInput}/>
                <input id="pass" name={'password'} type="password"  placeholder="رمز عبور" onChange={this.setInput}/>
                <a class="btn" id="submit" onClick={this.submitForm}>ورود</a>
                <div class="d-flex flex-row justify-content-between align-items-center" id="linksWrapper">
                    <p class="d-flex flex-row justify-content-center align-items-center" id="forgot"><a href="#">فراموشی  پسورد</a></p>
                    <p class="d-flex flex-row justify-content-center align-items-center" id="forgot"><Link to={'/signup'}>ثبت نام</Link></p>
                </div>
                </form>
            </div>
            <div className="d-flex justify-content-center align-items-center">
                <GoogleLogin
                    clientId="703688163019-tu8boquvieft42i7akus28qglu8d44n8.apps.googleusercontent.com"
                    buttonText="Login"
                    onSuccess={this.responseGoogle}
                    onFailure={ () => {alert('لطفا دوباره تلاش کنید') }}
                    cookiePolicy={'single_host_origin'}
                />
            </div>
        </div>
    );
    }
}  

const mapDispatchToProps = dispatch => {
	return {
		makeLogin: (data, history) => {
			dispatch(thunk.thunkMakeLogin(data, history, true));
        },
        makeGoogleSignup: (data) => {
            dispatch(thunk.thunkGoogleSignUp(data, true));
        }
	};
};


export default withRouter(connect(null, mapDispatchToProps)(LoginPage))