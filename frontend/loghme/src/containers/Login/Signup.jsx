import React from "react"
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import thunk from "../../thunk/ThunkApiMainHandeler";


class SignupPage extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            email: null, 
            password: null,
            name: null,
            last_name: null,
            phone_number: null
        }
    }

    setInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    submitForm = (e) => {
        e.preventDefault();
        this.props.makeSignUp(this.state, this.props.history)
    }

    render(){
    return(
        <div id="loginWrapper" class="container-fluid d-flex justify-content-center align-items-center">
            <div id="main" class="row d-flex flex-column justify-content-start align-items-center"> 
            <p id="sign" > ! ثبت نام در لقمه</p>
            <form id="form1">
              <input id="username" type="text" name={'name'} placeholder="نام" onChange={this.setInput}/>
              <input id="username" type="text"  name={'last_name'} placeholder="نام خانوادگی" onChange={this.setInput}/>
              <input id="username" type="text"  name={'phone_number'} placeholder="شماره موبایل" onChange={this.setInput}/>
              <input id="username" type="email"  name={'email'} placeholder="ایمیل" onChange={this.setInput}/>

              <input id="pass" type="password" name={'password'}  placeholder="کلمه عبور" onChange={this.setInput}/>
              <input id="pass" type="password"  placeholder="تایید کلمه عبور"/>
              <a class="btn" onClick={this.submitForm} id="signupSubmit">ثبت نام</a>
            </form>
        </div>
        </div>
    );
    }
}

const mapDispatchToProps = dispatch => {
	return {
		makeSignUp: (data, history) => {
			dispatch(thunk.thunkMakeSignUp(data, history, true));
        }
	};
};

export default withRouter(connect(null, mapDispatchToProps)(SignupPage))