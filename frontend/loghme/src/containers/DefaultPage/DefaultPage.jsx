import React, { Component } from 'react';

class DefaultPageWrapper extends Component {
    render() {
        return (
        <div className="jumbotron mx-4 p-0" id="defaultPageWrapper">
            <div className="row w-100">
                <div className="" id="descriptionPart">
                    <h1>صفحه مورد نظر در دست ساخت میباشد</h1>
                </div>
                <div className="col-md-6 w-100"  id="imagePart">
                </div>
            </div>
        </div>
        );
    }
}


export default DefaultPageWrapper;
