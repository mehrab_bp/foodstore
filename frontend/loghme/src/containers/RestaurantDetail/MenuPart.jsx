import React from "react";
import start from "./images/star.svg";
import chicken from './images/chicken.jpeg';

function Food({data, addToCart}){
    return(
        <div className="col-xl-4 col-lg-6 col-md-8">
            <div className="restaurant-food-card">
                <img className="food-picture" alt="food-pic" src={data.image} />

                <spn className="food-name-detail-container">
                    <div className="food-name ml-2">
                        {data.name}
                    </div>
                    <span className="food-popularity-number ml-1">
                        {Math.round(data.popularity * 5)}
                    </span>
                    <img src={start} alt="logo" className="food-popularity mb-1" />
                </spn>
                <div className="food-price d-flex flex-row-reverse justify-content-center">
                    <span className="ml-1">{data.price}</span>
                    <span>تومان</span>
                </div>
                {
                    // data.count ? 
                    <span className="inventory-btn available" onClick={() => addToCart(data)}>افزودن به سبد خرید</span>
                    // :<span className="inventory-btn unavailable">ناموجود</span>
                }
                
            </div>
        </div>
    )
}

function Menu({menu, addToCart}){
    if(Array.isArray(menu)){
        return(
            <div id="foodsListWrapper" className="row flex-row-reverse align-items-center justify-content-lg-start justify-content-md-center">
                {menu.map(item => (
                    <Food data={item} addToCart={addToCart}/>
                ))}
            </div>
        )        
    }else{
        return(
            <div id="foodsListWrapper" className="row align-items-center justify-content-lg-start justify-content-md-center">
            </div>
        )
    }
}

export default class MenuPart extends React.Component{
   
   
   
    render(){
        return(
            <div id="menuContainer" className="col-lg-8 col-md-12">
                <span className="restaurant-menu-title">منـو غذا</span>    
                <Menu menu={this.props.menu} addToCart={this.props.addToCart}/>
            </div>
        )
    }
}