import React from "react";



function Order({data, addToCart, reduceFromCart}){
    return (
            <li className="cart-item">
                <div className="cart-order">
                    <span className="food-name">
                       {data.foodName}
                    </span>
                    <span>
                        <i className="flaticon-minus cart-decrease-btn btn" onClick={() => reduceFromCart(data.id)}></i>
                        {data.number}
                        <i className="flaticon-plus cart-increase-btn btn" onClick={() => addToCart(data)}></i>
                    </span>
                </div>
                <p>
                    {data.price}
                </p>
            </li>
    )
}

export default class CartPart extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            totalPrice: 0
        }
    }

    calcTotalPrice(){
        let sum = 0;
        this.props.cart.orders.map(item => {
            sum += item.price
        })
        this.setState({
            totalPrice: sum
        })
    }

    componentDidUpdate(prevProps){
        if(prevProps.cart !== this.props.cart){
            this.calcTotalPrice()
        }
    }

    componentDidMount(){
        this.calcTotalPrice()
    }

    finalizeOrder = (e) => {
        e.preventDefault();
        this.props.finalizeOrder()
    }

    addToCart = (food) => {
        let data = {
            foodName: food.foodName,
            restaurantName: this.props.cart.restaurantName,
            restaurantId: this.props.cart.restaurantId,
            foodPrice: food.price
        }
        this.props.addToCart(data)
    }   

    render(){
        return(
            <div id="cartWrapper" className="col-lg-4 col-md-12">
                    <div id="cart">
                        <span className="cart-title">سبد خرید</span>
                        <ul className="cart-items-container">
                            {
                                this.props.cart.orders.map(item => (
                                    <Order data={item} reduceFromCart={this.props.reduceFromCart} addToCart={this.addToCart}/>
                                ))
                            }                            
                        </ul>
                        <div>
                            <span>جمع کل : </span>
                            <span className="cart-total">
                                {this.state.totalPrice}
                            </span>
                            <span className="cart-total">تومان</span>
                        </div>
                        <div className="submit-order-btn btn" onClick={this.finalizeOrder}>تایید نهایی</div>
                    </div>
    
            </div>
        )
    }
}