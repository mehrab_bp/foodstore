import React from 'react'
import HeaderPart from './HeaderPart';
import ContentWrapper from './ContentWrapper'
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import thunk from '../../thunk/ThunkApiMainHandeler';
class RestaurantsDetailWrapper extends React.Component{
    componentDidMount(){
        let desiredId = this.props.match.params.id
        console.log(this.props.pickedRestaurant)
        if(this.props.pickedRestaurant.id !== desiredId){
            this.props.setSpecRestuarant(desiredId)
        }
    }

    render(){
        return (
            <div id="restaurantDetailPage">
                <HeaderPart name={this.props.pickedRestaurant.name} logo={this.props.pickedRestaurant.logo}/>
                <ContentWrapper pickedRestaurant={this.props.pickedRestaurant}/>
            </div>
        ) 
    }
}

const mapStateToProps = state => {
	return {
		pickedRestaurant: state.restaurants.pickedRestaurant,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		setSpecRestuarant: (res_id) => {
			dispatch(thunk.thunkGetSpecRestaurant(res_id, true));
		}
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RestaurantsDetailWrapper));