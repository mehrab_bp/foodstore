import React from "react";
import CartPart from './CartPart';
import MenuPart from './MenuPart'; 
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import thunk from '../../thunk/ThunkApiMainHandeler';
import FoodModal from '../../components/FoodModal/FoodModal'


class ContentWrapper extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            showFoodModal: false,
            selectedFood: {}
        }
        this.finalizeOrder = this.finalizeOrder.bind(this);
        this.clearCart = this.clearCart.bind(this);
    }

    componentDidMount(){
        if(this.props.profile === undefined){
            this.props.getProfile()
        }
    }

    componentDidUpdate(prevProps){
        if(this.props.profile !== prevProps.profile){
            console.log(this.props.profile)
        }
    }

    finalizeOrder(){
        this.props.finalizeOrder();
    }

    clearCart(){
        this.props.clearCart();
    }


    handleShow = () => {
        this.setState({
            showFoodModal: !this.state.showFoodModal
        })
    }

    selectFood = (food, restaurant) => {
        let data = food
        if(restaurant){
            data.restaurantName = restaurant.restaurantName
            data.restaurantId = restaurant.restaurantId
        }else{
            data.restaurantName = this.props.pickedRestaurant.name
            data.restaurantId = this.props.pickedRestaurant.id         
        }
        this.setState({
            selectedFood: data  
        })
        this.handleShow()
    }

    addToCart = () => {
        this.handleShow()
        let finalData = {
            food_id: this.state.selectedFood.id,
            restaurant_id: this.state.selectedFood.restaurantId
        }
        this.props.addToCart(finalData)
    }

    addToCartDirectly = (data) => {
        this.props.addToCart(data)
    }

    reduceFromCart = (foodId) => {
        let data = {
            food_id: foodId
        }

        this.props.reduceFromCart(data)
    }

    render(){
        return(
            <div className="restaurant-page-content-container row flex-row-reverse">
                {(this.props.profile && this.props.profile.cart) ? 
                <CartPart cart={this.props.profile.cart} finalizeOrder={this.finalizeOrder} reduceFromCart={this.reduceFromCart} addToCart={this.addToCartDirectly}/>:""
                }
                <MenuPart addToCart={this.selectFood} menu={this.props.pickedRestaurant.menu}/>
                
                <FoodModal 
                    handleShow={this.handleShow}
                    data={this.state.selectedFood}
                    show={this.state.showFoodModal}
                    addToCart={this.addToCart}
                />

            </div>
        )
    }
}  

const mapStateToProps = state => {
	return {
		profile: state.user.profile,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		addToCart: (data) => {
			dispatch(thunk.thunkAddToCart(data, true));
        },
        getProfile: () => {
            dispatch(thunk.thunkGetUserProfile("", true))
        },
        finalizeOrder: () => {
            dispatch(thunk.thunkFinalizeCart(true))
        },
        clearCart: () => {
            dispatch(thunk.thunkClearCart(true))
        },
        reduceFromCart: (data) => {
            dispatch(thunk.thunkReduceFromCart(data, true))
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentWrapper));