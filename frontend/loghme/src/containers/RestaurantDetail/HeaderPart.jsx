import React from 'react'
// import logo from './images/hot-pizza.jpg'

export default function Header(props){
    return (
        <div id="restaurantCover">
                <div className="restaurant-cover"></div>
                <div className="restaurant-detail-container">
                    <img src={props.logo} alt="log" className="restaurant-logo" />
                    <p className="restaurant-name">
                        {props.name}
                    </p>
                </div>
        </div>
    )
}

