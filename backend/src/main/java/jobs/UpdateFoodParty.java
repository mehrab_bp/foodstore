package jobs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import database.DataBaseAccess;
import models.FoodInfo.FoodInfo;
import models.FoodInfo.FoodInfoMapper;
import models.FoodParty.FoodParty;
import models.FoodParty.FoodPartyMapper;
import models.Restaurant.RestaurantInfo;
import models.Restaurant.RestaurantMapper;
import models.RestaurantParty;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import util.GlobalFunc;
import util.RestaurantUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class UpdateFoodParty implements Runnable {
    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    @Override
    public void run() {
        String res = null;
        List<RestaurantParty> restaurants = null;
        try {
            res = GlobalFunc.SendGetReq("http://138.197.181.131:8080/foodparty", httpClient);
            restaurants = new Gson().fromJson(res, new TypeToken<List<RestaurantParty>>(){}.getType());
            DataBaseAccess.getInstance().setRestaurantsInFoodParty(restaurants);
            initialParties(restaurants);
            System.out.println("updating foodParty ...");
        } catch (Exception e) {

        }
    }

    public void initialParties(List<RestaurantParty> parties) {
        try {
            for (FoodParty foodParty : FoodPartyMapper.getInstance().getAllData()) {
                FoodInfoMapper.getInstance().delete(foodParty.getId());
            }
            FoodPartyMapper.getInstance().deleteAllData();
            RestaurantMapper restaurantMapper = RestaurantMapper.getInstance();
            for (RestaurantInfo restaurant : restaurantMapper.getParties()) {
                restaurantMapper.delete(restaurant.getId());
            }

            for (RestaurantParty party : parties) {
                if(party != null){
                    RestaurantInfo restaurantInfo = RestaurantMapper.getInstance().find(party.getId());
                    if (restaurantInfo == null) {
                        RestaurantInfo newRestaurant = new RestaurantInfo(party.getId(),
                                party.getName(), party.getLocation(), party.getLogo());
                        newRestaurant.setFoodParty(true);
                        RestaurantUtils.setEstimated(newRestaurant);
                        RestaurantMapper.getInstance().insert(newRestaurant);
                    } else {
                        RestaurantMapper.getInstance().updateFoodParty(restaurantInfo.getId(), true);
                    }
                    for (FoodParty food : party.getMenu()) {
                        food.setRestaurantId(party.getId());
                        food.setId(UUID.randomUUID().toString());
                        food.setFoodParty(true);
                        FoodInfoMapper.getInstance().insert(food);
                        FoodPartyMapper.getInstance().insert(food);
                    }
                }else{
                    System.out.println("error <-----------------");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}