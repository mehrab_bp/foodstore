package jobs;

import database.DataBaseAccess;
import models.FinalizedOrder.FinalizedOrder;
import models.User.User;
import models.User.UserMapper;
import util.DeliveryUtils;

public class FindSomeDeliver implements Runnable {
    @Override
    public void run() {
        try {
            for (User user : UserMapper.getInstance().getAllUsers()) {
                for (FinalizedOrder finalizedOrder : user.getFinalizedOrders()) {
                    if (finalizedOrder.getStatus().equals(FinalizedOrder.DeliveryStatus.findingDelivery)) {
                        DeliveryUtils.getNearestDelivery(finalizedOrder);
                        System.out.println("try to find some deliver for orderId: " + finalizedOrder.getId());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}