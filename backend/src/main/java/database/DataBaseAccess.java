package database;

import models.Delivery;
import models.Restaurant.RestaurantInfo;
import models.RestaurantParty;
import models.User.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class DataBaseAccess {
    private Set<RestaurantInfo> restaurants;
    private List<User> users;
    private List<Delivery> deliveries;
    private List<RestaurantParty> restaurantsInFoodParty;
    private static DataBaseAccess instance = null;
    private String defaultUserId = UUID.randomUUID().toString();

    private DataBaseAccess() {

    }

    public static DataBaseAccess getInstance() {
        if (instance == null) {
            instance = new DataBaseAccess();
        }
        return instance;
    }

    public Set<RestaurantInfo> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(Set<RestaurantInfo> restaurants) {
        this.restaurants = restaurants;
    }


    public List<User> getUsers() throws SQLException {
        return User.getAllUsers();
    }

//    public void setUsers(List<User> users) {
//        this.users = users;
//    }

    public List<Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    public List<RestaurantParty> getRestaurantsInFoodParty() {
        return restaurantsInFoodParty;
    }

    public void setRestaurantsInFoodParty(List<RestaurantParty> restaurantsInFoodParty) {
        this.restaurantsInFoodParty = restaurantsInFoodParty;
    }

    public void addRestaurant(RestaurantInfo restaurantInfo) {
        this.restaurants.add(restaurantInfo);
    }
}
