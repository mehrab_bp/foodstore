package database.driver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Mapper<T, I> implements IMapper<T, I> {

    protected Map<I, T> loadedMap = new HashMap<I, T>();

    abstract protected String getFindStatement(I id);

    abstract protected String getInsertStatement();

    abstract protected String getDeleteStatement(I id);

    abstract protected T convertResultSetToObject(ResultSet rs) throws SQLException;

    public T find(I id) throws SQLException {
        T result = loadedMap.get(id);
        if (result != null)
            return result;

        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getFindStatement(id))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(!resultSet.next()) {
                    resultSet.close();
                    st.close();
                    con.close();
                    return null;
                }
                return convertResultSetToObject(resultSet);
            } catch (SQLException ex) {
                System.out.println("error in Mapper.findByID query: " + ex.getMessage());
                throw ex;
            }
        }
    }

    abstract public void fillInsertStatement(PreparedStatement stmt, T object) throws SQLException;

    public void insert(T object) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getInsertStatement())
        ) {
            try {
                fillInsertStatement(st, object);
                st.executeUpdate();
                st.close();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.insert query.");
                throw ex;
            }
        }
    }

    public void delete(I id) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getDeleteStatement(id))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.delete query.");
                throw ex;
            }
        }
    }

    public void deleteByStatement(String statement) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(statement)
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.delete query.");
                throw ex;
            }
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            throw sqlException;
        }
    }

    public List<T> executeListReturner(String statement) throws SQLException {
        List<T> result = new ArrayList<T>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(statement);
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while (resultSet.next())
                    result.add(convertResultSetToObject(resultSet));
                return result;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                throw ex;
            }
        }
    }

    public ArrayList<T> findListWithPagination(String query, int pageNumber, int pageSize) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement stmt = con.prepareStatement(query + getPaginationStatement() )
        ) {
            stmt.setInt(1, pageSize);
            stmt.setInt(2, ( ( pageNumber - 1 ) * pageSize ));
            ResultSet resultSet;
            resultSet = stmt.executeQuery();
            ArrayList<T> result = getListFromResultSet(resultSet);
            stmt.close();
            con.close();
            return result;
        }
    }

    private ArrayList<T> getListFromResultSet(ResultSet resultSet) throws SQLException {
        ArrayList<T> result = new ArrayList<>();
        while(resultSet.next()) {
            result.add(convertResultSetToObject(resultSet));
        }
        resultSet.close();
        return result;
    }

    public void update(String statement) throws SQLException{
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(statement);
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.update query.");
                throw ex;
            }
        }
    }


    public T executeSingleReturner(String statement) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(statement)
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(!resultSet.next()) {
                    resultSet.close();
                    st.close();
                    con.close();
                    return null;
                }
                return convertResultSetToObject(resultSet);
            } catch (SQLException ex) {
                System.out.println("error in Mapper.find single obj query.");
                throw ex;
            }
        }
    }

    protected String getPaginationStatement() {
        return "LIMIT ?  OFFSET ? ";
    }
}