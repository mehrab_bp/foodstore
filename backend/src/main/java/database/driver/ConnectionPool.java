package database.driver;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbcp.BasicDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionPool {

    private static BasicDataSource ds = new BasicDataSource();

    static {
        ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
        ds.setUrl("jdbc:mysql://mysql-service:3306/iedb?useUnicode=yes&characterEncoding=UTF-8&autoReconnect=true&useSSL=false");
        ds.setUsername("root");
        ds.setPassword("mysql123");
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
    }

    public static Connection getConnection() throws SQLException {
        while (true) {
            try {
                return ds.getConnection();
            } catch (SQLException e) {
                System.out.println("Another try to connect to database...");
                e.printStackTrace();
            }
        }
    }

    private ConnectionPool() {
    }
}