package util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import exceptions.NotFoundException;
import models.*;
import models.FinalizedOrder.FinalizedOrderMapper;
import models.Location;
import models.Restaurant.RestaurantInfo;
import models.FinalizedOrder.FinalizedOrder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.sql.SQLException;
import java.util.*;

public class DeliveryUtils {
    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    public static void reduceDeliveryTenSecond(FinalizedOrder finalizedOrder) throws SQLException {
        if (finalizedOrder.getDeliveryTime().hasFinished()) {
            finalizedOrder.setStatus(FinalizedOrder.DeliveryStatus.done);
            FinalizedOrderMapper.getInstance().updateStatus(finalizedOrder.getId(),
                    FinalizedOrder.DeliveryStatus.done.ordinal(), new Time(0, 0).toString());
        } else {
            finalizedOrder.getDeliveryTime().decreaseTenSec();
            FinalizedOrderMapper.getInstance().updateStatus(finalizedOrder.getId(),
                    FinalizedOrder.DeliveryStatus.delivering.ordinal(), finalizedOrder.getDeliveryTime().toString());
        }
    }

    public static void getNearestDelivery(FinalizedOrder finalizedOrder) throws NotFoundException, SQLException {
        List<Delivery> deliveries = getDeliveries();
        if (deliveries == null || deliveries.isEmpty()) {
            finalizedOrder.setStatus(FinalizedOrder.DeliveryStatus.findingDelivery);

            finalizedOrder.setDeliveryTime(
                    new Time((int)Math.floor(3600/60.0), (int)Math.floor(3600%60)));
        } else {
            HashMap<Delivery, Double> deliveryTime = new HashMap<>();
            calculateDeliveriesTime(deliveryTime, deliveries,
                    RestaurantUtils.getRestaurantById(finalizedOrder.getRestaurantId()));
            HashMap<Delivery, Double> sortedDeliveryTime = sortByValue(deliveryTime);
            finalizedOrder.setStatus(FinalizedOrder.DeliveryStatus.delivering);
            Double minimumDeliveryTime = findingMinimumTime(sortedDeliveryTime);
            finalizedOrder.setDeliveryTime(
                    new Time((int)Math.floor(minimumDeliveryTime/60.0), (int)Math.floor(minimumDeliveryTime%60)));
            FinalizedOrderMapper.getInstance().updateStatus(finalizedOrder.getId(),
                    FinalizedOrder.DeliveryStatus.delivering.ordinal(), finalizedOrder.getDeliveryTime().toString());
        }
    }

    public static List<Delivery> getDeliveries() {
        String res = null;
        List<Delivery> deliveries = null;
        try {
            res = GlobalFunc.SendGetReq("http://138.197.181.131:8080/deliveries", httpClient);
            deliveries = new Gson().fromJson(res, new TypeToken<List<Delivery>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    public static void calculateDeliveriesTime(HashMap<Delivery, Double> deliveryTime,
                                               List<Delivery> deliveries, RestaurantInfo restaurantInfo) {
        for (Delivery delivery : deliveries) {
            Double time = findTime(delivery, restaurantInfo.getLocation());
            deliveryTime.put(delivery, time);
        }
    }

    public static Double findTime(Delivery delivery, Location restaurantLoc) {
        return (Math.sqrt(Math.pow(restaurantLoc.getX(), 2)
                + Math.pow(restaurantLoc.getY(), 2))
                + Math.sqrt(Math.pow(restaurantLoc.getX() - delivery.getLocation().getX(), 2)
                + Math.pow(restaurantLoc.getY() - delivery.getLocation().getY(), 2)))
                / delivery.getVelocity();
    }

    public static HashMap<Delivery, Double> sortByValue(HashMap<Delivery, Double> hm)
    {
        List<Map.Entry<Delivery, Double> > list = new LinkedList<Map.Entry<Delivery, Double> >(hm.entrySet());
        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<Delivery, Double> >() {
            public int compare(Map.Entry<Delivery, Double> o1,
                               Map.Entry<Delivery, Double> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<Delivery, Double> temp = new LinkedHashMap<Delivery, Double>();
        for (Map.Entry<Delivery, Double> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    public static Double findingMinimumTime(HashMap<Delivery, Double> sortedDeliveryTime) {
        Iterator<Delivery> iterator = sortedDeliveryTime.keySet().iterator();
        Double minimumDeliveryTime = null;
        if (iterator.hasNext())
            minimumDeliveryTime = sortedDeliveryTime.get(iterator.next());
        return minimumDeliveryTime;
    }
}
