package util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;

public class JWTUtil {
    private final static String secret = "loghme";
    public static final String HEADER_PARAM = "x-access-token";
    public static final Integer ttlMillis = 86400000;
    public static String createToken(String userId) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        long expMillis = nowMillis + ttlMillis;
        Date exp = new Date(expMillis);
        try {
            return JWT.create()
                    .withIssuer("auth0")
//                    .withClaim("iss", true)
                    .withClaim("iat", now)
                    .withClaim("exp", expMillis)
                    .withClaim("id", userId)
                    .withSubject("json-web-token") //todo what subject is? and how to implement correct subject
                    .sign(Algorithm.HMAC256(secret));
        } catch (JWTCreationException exception) {
            throw new RuntimeException("You need to enable Algorithm.HMAC256");
        }
    }

    public static String getUserIdInToken(String token){
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret))
                    .withIssuer("auth0")
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getClaim("id").asString();
        } catch (JWTDecodeException exception) {
            return null;
        }
    }
}