package util;

import database.DataBaseAccess;
import exceptions.NotEnoughFoodException;
import exceptions.NotFoundException;
import exceptions.NotInAreaException;
import exceptions.TimeLimitException;
import models.*;
import models.FoodInfo.FoodInfo;
import models.FoodParty.FoodParty;
import models.FoodParty.FoodPartyMapper;
import models.Location;
import models.Restaurant.RestaurantInfo;
import models.Restaurant.RestaurantMapper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RestaurantUtils {
    public static List<RestaurantInfo> findRestaurantsInArea() {
        List<RestaurantInfo> restsInArea = new ArrayList<>();
        for (RestaurantInfo restaurantInfo : DataBaseAccess.getInstance().getRestaurants()) {
            if (distance(restaurantInfo.getLocation()) <= 170.0) {
                restsInArea.add(restaurantInfo);
            }
        }
        return restsInArea;
    }

    public static Double distance(Location loc) {
        return Math.sqrt(Math.pow(loc.getX().doubleValue(), 2) + Math.pow(loc.getY().doubleValue(), 2));
    }

    public static RestaurantInfo getRestaurantById(String restaurantId) throws NotFoundException, SQLException {
        RestaurantInfo restaurantInfo = RestaurantInfo.getRestaurantWithMenuById(restaurantId);
        if(restaurantInfo != null){
            return restaurantInfo;
        }else{
            throw new NotFoundException("There is no restaurant with id: " + restaurantId);
        }
    }

    public static FoodInfo getSingleOneByRestaurantId(String restaurantId, String foodId)
            throws SQLException, NotFoundException {
        FoodInfo foodInfo = FoodInfo.getSingleOneByRestaurantId(restaurantId, foodId);
        if(foodInfo != null){
            return foodInfo;
        }else{
            throw new NotFoundException("There is no food with id: " + foodId);
        }
    }

    public static List<RestaurantInfo> getAllRestaurants(Integer pageNumber, Integer pageSize) throws SQLException {
        return RestaurantInfo.getAllRestaurants(pageNumber, pageSize);
    }

    public static  List<RestaurantInfo> searchWithRestaurantName(Integer pageNumber, Integer pageSize, String search)
            throws SQLException {
        return RestaurantInfo.getSearchedRestaurantsByName(pageNumber, pageSize, search);
    }

    public static  List<RestaurantInfo> searchWithFoodName(Integer pageNumber, Integer pageSize, String search)
            throws SQLException {
        return RestaurantInfo.getSearchedRestaurantsByFoodName(pageNumber, pageSize, search);
    }

//    public static RestaurantParty getRestaurantPartyById(String id) throws TimeLimitException {
//        for (RestaurantParty restaurantParty : RestaurantMapper.getInstance().) {
//            if (restaurantParty.getId().equals(id))
//                return restaurantParty;
//        }
//        throw new TimeLimitException("This restaurant is no longer ordering.");
//    }

    public static void setRestaurantParties(List<RestaurantParty> res) {
        for (RestaurantParty restaurantParty : res) {
            try {
                checkRestaurantExistence(restaurantParty.getId());
            } catch (Exception e) {
                DataBaseAccess.getInstance().addRestaurant(new RestaurantInfo(
                        restaurantParty.getId(), restaurantParty.getName(),
                        restaurantParty.getLocation(), restaurantParty.getLogo()));
            }

        }
    }

    public static void checkRestaurantArea(RestaurantInfo restaurantInfo) throws NotInAreaException {
        if (distance(restaurantInfo.getLocation()) > 170.0) {
            throw new NotInAreaException("restaurant with id: " + restaurantInfo.getId() + " is not in your area!");
        }
    }

    public static void checkRestaurantExistence(String restaurantId) throws NotFoundException {
        for (RestaurantInfo restaurantInfo : DataBaseAccess.getInstance().getRestaurants()) {
            if (restaurantInfo.getId().equals(restaurantId)) {
                return;
            }
        }
        throw new NotFoundException("There is no restaurant with id: " + restaurantId);
    }

    public static void checkIfSuchFoodsExistInRestaurantMenu(String foodName, String restaurantId)
            throws NotFoundException, SQLException {
        RestaurantInfo restaurantInfo = getRestaurantById(restaurantId);
        for (FoodInfo foodInfo : restaurantInfo.getMenu()) {
            if (foodInfo.getName().equals(foodName)) {
                return;
            }
        }
        throw new NotFoundException("no such a food");
    }

    public static void setEstimated() {
        for (RestaurantInfo restaurantInfo : DataBaseAccess.getInstance().getRestaurants()) {
            Double estimatedDistance = 1.5 * distance(restaurantInfo.getLocation());
            Double estimatedTime = (estimatedDistance / 5) + 60;
            restaurantInfo.setEstimatedDelivery(new Time(
                    (int)Math.floor(estimatedTime/60.0),
                    (int)Math.floor(estimatedTime%60)));
        }
    }

    public static void setEstimated(RestaurantInfo res) {
        Double estimatedDistance = 1.5 * distance(res.getLocation());
        Double estimatedTime = (estimatedDistance / 5) + 60;
        res.setEstimatedDelivery(new Time(
                (int)Math.floor(estimatedTime/60.0),
                (int)Math.floor(estimatedTime%60)));
    }

    public static void decreaseFoodNumberInRestaurant(String id, String name, Integer number)
            throws TimeLimitException, NotEnoughFoodException, SQLException {
        List<FoodParty> parties = FoodPartyMapper.getInstance().getFoodPartiesByRestaurantId(id);
        for (FoodParty foodParty : parties) {
            if (foodParty.getName().equals(name)) {
                if (foodParty.getCount() < number)
                    throw new NotEnoughFoodException("there is not enough food " + name + " in food party.");
                System.out.println("already has " + foodParty.getCount() + " and " + number + " should decrease");
                FoodPartyMapper.getInstance().updateCount(foodParty.getId(), foodParty.getCount() - number);
            }
        }
    }

    public static List<RestaurantParty> findParties() throws SQLException {
        List<RestaurantInfo> restaurantInfos = RestaurantMapper.getInstance().getParties();
        List<RestaurantParty> parties = new ArrayList<>();
        for (RestaurantInfo restaurantInfo : restaurantInfos) {
            List<FoodParty> foodParties = FoodPartyMapper.getInstance().getFoodPartiesByRestaurantId(
                    restaurantInfo.getId());
            RestaurantParty party = new RestaurantParty(restaurantInfo.getId(), restaurantInfo.getName(),
                    restaurantInfo.getLocation(), restaurantInfo.getLogo(), foodParties);
            parties.add(party);
        }
        return parties;
    }
}
