package util;

import com.github.mfathi91.time.PersianDate;
import com.google.common.hash.Hashing;
import database.DataBaseAccess;
import exceptions.*;
import models.Cart.Cart;
import models.Cart.CartMapper;
import models.FinalizedOrder.FinalizedOrder;
import models.User.User;
import models.User.UserMapper;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class UserUtils {


    public static User findUserById(String id) throws NotFoundException, SQLException {
        User user = User.getUserById(id);
        if(user != null){
            return user;
        }else{
            throw new NotFoundException("there is no user with id: " + id);
        }
    }

    public static User inCreaseCredit(Integer amount, String userId) throws NotFoundException, SQLException {
        User user = UserUtils.findUserById(userId);
        if(user == null){
            throw new NotFoundException("there is no user with id: " + userId);
        }
        user.setCredit(amount + user.getCredit());
        user.setCreditToDb();
        return user;
    }

    public static List<User> getAllUsers() throws SQLException {
        return User.getAllUsers();
    }

    public static String addFinalizedOrder(User user) throws NotFoundException, SQLException {
        FinalizedOrder finalizedOrder = new FinalizedOrder(UUID.randomUUID().toString());
        Cart cart = Cart.getCartByItsOrdersFromDBByUserId(user.getId());
        finalizedOrder.setOrders(cart.getOrders());
        finalizedOrder.addOrdersInDB(cart.getOrders());
        finalizedOrder.setRestaurantName(cart.getRestaurantName());
        finalizedOrder.setRestaurantId(cart.getRestaurantId());
        finalizedOrder.setDate(PersianDate.now());
        finalizedOrder.setUserId(user.getId());
        DeliveryUtils.getNearestDelivery(finalizedOrder);
        user.addFinalizeOrder(finalizedOrder);
        try{
            finalizedOrder.saveThis();
            cart.detachOrdersFromCart();
            cart.updateRestaurantInDB("", "");
        }catch (Exception e){
            e.printStackTrace();
        }
        return finalizedOrder.getId();
    }

    public static FinalizedOrder getOrderById(String id, String userId) throws NotFoundException, SQLException {
        FinalizedOrder finalizedOrder = FinalizedOrder.findSingleByUserId(userId, id);
        if(finalizedOrder != null){
            return finalizedOrder;
        }else{
            throw new NotFoundException("there is no order with id: " + id);
        }
    }

    public static void finalizeOrderForUser(User user) throws NotEnoughCreditException, NotEnoughFoodException,
            SQLException, TimeLimitException, EmptyCartException, NotFoundException {
        CartUtils.checkingFinalizeConditions(user);
        CartUtils.decresCreditFromUser(user);
        UserUtils.addFinalizedOrder(user);
    }

    public static User createNewUser(String email, String password, String name, String lastname, String phoneNumber)
            throws SQLException, DuplicateException {
        User user = User.getUserByEmail(email);
        if(user != null){
            throw new DuplicateException("Already there is user with email: " + email);
        }
        String id = UUID.randomUUID().toString();
        String hashPassword = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
        user =  new User(id,name, lastname, phoneNumber, email, 0, hashPassword);
        user.saveThis();

        Cart cart = new Cart(UUID.randomUUID().toString(), "", "", id);
        CartMapper.getInstance().insert(cart);
        return user;
    }

    public static String makeUserLogin(String email , String password) throws NotFoundException, SQLException,
            LoginFailure {
        User user = User.getUserByEmail(email);
        if(user == null){
            throw new NotFoundException("there is no user with email: " + email);
        } else {
            String hashPassword = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
            if (!user.getPassword().equals(hashPassword)) {
                throw new LoginFailure("password is incorrect!");
            }
        }
        //todo should authenticate password with bcrypt
        return JWTUtil.createToken(user.getId());
    }
}
