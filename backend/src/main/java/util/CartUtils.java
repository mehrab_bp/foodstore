package util;

import database.DataBaseAccess;
import exceptions.*;
import models.Cart.Cart;
import models.FoodInfo.FoodInfo;
import models.Order.Order;
import models.Restaurant.RestaurantInfo;
import models.Restaurant.RestaurantMapper;
import models.RestaurantParty;
import models.User.User;
import models.User.UserMapper;

import java.sql.SQLException;
import java.util.List;

public class CartUtils {
    public static Cart getCartWithOrdersByUserId(String userId) throws SQLException, NotFoundException {
        Cart cart = Cart.getCartByItsOrdersFromDBByUserId(userId);
        if(cart == null){
            throw new NotFoundException("cart not found");
        }
        return cart;
    }

    public static Cart clearCartByUserId(String userId) throws NotFoundException, SQLException {
        Cart cart = CartUtils.getCartWithOrdersByUserId(userId);
        cart.deleteCartOrdersFromDB();
        cart.updateRestaurantInDB("", "");
        cart.clear();
        return cart;
    }

    public static void checkingFinalizeConditions(User user) throws EmptyCartException, NotEnoughCreditException,
            NotEnoughFoodException, TimeLimitException, SQLException {
        if (user.getCart().getOrders().isEmpty()) {
            throw new EmptyCartException("your cart is Empty!");
        } else if (factor(user.getCart().getOrders()) > user.getCredit()) {
            throw new NotEnoughCreditException("your credit is not enough, please charge your credit!");
        }
        if (itIsFoodParty(user.getCart().getRestaurantId())) {
            for (Order order : user.getCart().getOrders()) {
                System.out.println();
                RestaurantUtils.decreaseFoodNumberInRestaurant(
                        user.getCart().getRestaurantId(),
                        order.getFoodName(),
                        order.getNumber());
            }
        }
    }

    public static boolean itIsFoodParty(String id) throws SQLException {
        RestaurantInfo restaurantInfo =  RestaurantMapper.getInstance().find(id);
        if (restaurantInfo.isFoodParty())
            return true;

        return false;
    }

    public static Integer factor(List<Order> orders) throws SQLException {
        Integer sumOfOrders = 0;
        for (Order order : orders) {
            sumOfOrders += order.getPrice();
        }
        return sumOfOrders;
    }

    public static void decresCreditFromUser(User user) throws SQLException{
        UserMapper.getInstance().updateCredit(user.getId(),
                user.getCredit() - factor(user.getCart().getOrders()));
    }

    public static void addToCart(String foodId, String restaurantId, User user)
            throws DuplicateException, SQLException, NotFoundException {
        RestaurantInfo restaurantInfo = RestaurantUtils.getRestaurantById(restaurantId);
        FoodInfo foodInfo = RestaurantUtils.getSingleOneByRestaurantId(restaurantId, foodId);
        Cart cart = Cart.findByUserId(user.getId());
        if (cart.getRestaurantName().isEmpty()) {
            cart.setRestaurantName(restaurantInfo.getName());
            cart.addToOrder(foodInfo.getName(), foodInfo.getPrice());
            cart.setRestaurantId(restaurantInfo.getId());
            cart.updateRestaurantInDB(restaurantInfo.getName(), restaurantInfo.getId());
        } else if (!cart.getRestaurantName().equals(restaurantInfo.getName())) {
            throw new DuplicateException(
                    "You have food from " +
                    cart.getRestaurantName() +
                    " restaurant in your Cart!");
        } else {
            cart.addToOrder(foodInfo.getName(), foodInfo.getPrice());
        }
    }

    public static void reduceFromCart(String foodId, String userId) throws EmptyCartException, SQLException, NotFoundException {
        Cart cart = Cart.findByUserId(userId);
        FoodInfo foodInfo = RestaurantUtils.getSingleOneByRestaurantId(cart.getRestaurantId(), foodId);
        if (!cart.getOrders().isEmpty()) {
            cart.reduceFromOrder(foodInfo.getName());
            if (cart.getOrders().isEmpty()) {
                cart.setRestaurantName("");
                cart.setRestaurantId("");
                cart.updateRestaurantInDB("", "");
            }
        } else {
            throw new EmptyCartException("your cart already is empty!");
        }
    }
}
