package util;

import models.Location;
import models.Time;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.time.LocalTime;

public class GlobalFunc {
    public static String SendGetReq(String uri, CloseableHttpClient httpClient) throws Exception{
        HttpGet request = new HttpGet(uri);

        String result = "";
        try (CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
//                System.out.println(result);
            }

        }
        return result;
    }

    public static Time convertTime(java.sql.Time time){
        LocalTime localTime = time.toLocalTime();
        return new Time(localTime.getMinute(), localTime.getSecond());
    }

    public static Time convertToTime(String timeStr) {
        Integer min = Integer.parseInt(timeStr.split(":")[0]);
        Integer sec = Integer.parseInt(timeStr.split(":")[1]);
        return new Time(min, sec);
    }

    public static Location convertToLocation(String locationStr) {
        Integer x = Integer.parseInt(locationStr.split(":")[0]);
        Integer y = Integer.parseInt(locationStr.split(":")[1]);
        return new Location(x, y);
    }

}
