package exceptions;

public class DuplicateException extends Exception {
    String msg;
    public DuplicateException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
