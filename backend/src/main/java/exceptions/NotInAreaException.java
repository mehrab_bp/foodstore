package exceptions;

public class NotInAreaException extends Exception {
    String msg;
    public NotInAreaException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
