package exceptions;

public class NotEnoughFoodException extends Exception {
    String msg;
    public NotEnoughFoodException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

