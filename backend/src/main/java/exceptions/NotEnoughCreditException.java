package exceptions;

public class NotEnoughCreditException extends Exception {
    String msg;
    public NotEnoughCreditException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
