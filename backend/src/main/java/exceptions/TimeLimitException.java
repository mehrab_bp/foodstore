package exceptions;

public class TimeLimitException extends Exception {
    String msg;
    public TimeLimitException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}