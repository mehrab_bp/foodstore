package exceptions;

public class EmptyCartException extends  Exception {
    String msg;
    public EmptyCartException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
