package exceptions;

public class LoginFailure extends Exception {
    String msg;
    public LoginFailure(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
