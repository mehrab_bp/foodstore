package web;

import exceptions.NotFoundException;
import models.User.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import util.JWTUtil;
import util.UserUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(filterName="securityFilter", urlPatterns="/*")
public class SecurityRequestFilter implements Filter {
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String USER_ID_KEY = "userId";

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        Boolean passFilter = false;

        if(req.getHeader(AUTHORIZATION_HEADER)  != null){
            String token = req.getHeader(AUTHORIZATION_HEADER);
            String userId = JWTUtil.getUserIdInToken(token);
            if(userId == null){
                ((HttpServletResponse) response).sendError(HttpStatus.UNAUTHORIZED.value(), "please use your authorization code");
            }else{
                try{
                    User user = UserUtils.findUserById(userId);
                    req.setAttribute("user", user);
                    passFilter = true;
                }catch (SQLException e){
                    ((HttpServletResponse) response).sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
                }catch (NotFoundException e){
                    ((HttpServletResponse) response).sendError(HttpStatus.UNAUTHORIZED.value(), "please use your authorization code");
                }
            }
        }
//        else if(req.getSession() != null){
//            String userId = (String) req.getSession().getAttribute(USER_ID_KEY);
//            try{
//                User user = UserUtils.findUserById(userId);
//                req.setAttribute("user", user);
//                passFilter = true;
//            }catch (SQLException e){
//                ((HttpServletResponse) response).setStatus(HttpStatus.INTERNAL_SERVER_ERROR.ordinal());
//            }catch (NotFoundException e){
//                ((HttpServletResponse) response).setStatus(HttpStatus.NOT_FOUND.ordinal());
//            }
//        }
        else{
            System.out.println(req.getServletPath());
            if(req.getServletPath().contains("/login") || req.getServletPath().contains("/sign-up")){
                //do nothing
                passFilter = true;
            }else{
                ((HttpServletResponse) response).sendError(HttpStatus.UNAUTHORIZED.value(), "please use your authorization code");
            }
        }

        if(passFilter){
            chain.doFilter(request, response);
        }else {
                return;
        }
    }
}
