package web;

import database.DataBaseAccess;
import exceptions.DuplicateException;
import exceptions.EmptyCartException;
import exceptions.NotFoundException;
import models.Cart.Cart;
import models.User.User;
import models.User.UserMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import util.CartUtils;
import util.UserUtils;

import java.sql.SQLException;

@RestController
public class CartController {
    @GetMapping(value = "/cart", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Cart> getCart(
            @RequestAttribute("user") User user
    ) {
        try {
            return new ResponseEntity<>(CartUtils.getCartWithOrdersByUserId(user.getId()), HttpStatus.OK);
        } catch (SQLException e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (NotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/clearCart")
    public ResponseEntity clearCart(
            @RequestAttribute("user") User user
    ) {
            try {
                return ResponseEntity.ok().body(CartUtils.clearCartByUserId(user.getId()));
            } catch (NotFoundException e) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
            }catch (SQLException e){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
    }

    @PostMapping("/addToCart")
    public ResponseEntity addToCart(
            @RequestParam(value = "food_id") String foodId,
            @RequestParam(value = "restaurant_id") String restaurantId,
            @RequestAttribute("user") User user
    ) {
        try {
            CartUtils.addToCart(foodId, restaurantId, user);
            return new ResponseEntity(HttpStatus.OK);
        }catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        }catch (DuplicateException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMsg());
        } catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PostMapping("/reduceFromCart")
    public ResponseEntity reduceFromCart(
            @RequestParam(value = "food_id") String foodId,
            @RequestAttribute("user") User user
    ) {
        try {
            CartUtils.reduceFromCart(foodId, user.getId());
            return new ResponseEntity(HttpStatus.OK);
        }catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        }catch (EmptyCartException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMsg());
        }catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
