package web;

import database.DataBaseAccess;
import exceptions.NotFoundException;
import models.Restaurant.RestaurantInfo;
import models.Restaurant.RestaurantMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import util.RestaurantUtils;

import java.sql.SQLException;
import java.util.Collection;

@RestController
public class RestaurantController {

    @GetMapping(value = "/restaurants", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllRestaurants(
            @RequestParam(value = "pageNumber", required = false, defaultValue = "1") String pageNumber,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") String pageSize
    ) {
        try {
            return ResponseEntity.ok().body(RestaurantUtils.getAllRestaurants(
                    Integer.parseInt(pageNumber),
                    Integer.parseInt(pageSize)
            ));
        } catch (SQLException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/restaurants/foodSearch", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllRestaurantsWithFoodSearch(
            @RequestParam(value = "search", required = false) String foodSearch,
            @RequestParam(value = "pageNumber", required = false, defaultValue = "1") String pageNumber,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") String pageSize
    ) {
        try {
            if (foodSearch == null || foodSearch.isEmpty()) {
                return ResponseEntity.ok().body(RestaurantUtils.getAllRestaurants(
                        Integer.parseInt(pageNumber),
                        Integer.parseInt(pageSize)
                ));
            } else {
                return ResponseEntity.ok().body(RestaurantUtils.searchWithFoodName(
                        Integer.parseInt(pageNumber),
                        Integer.parseInt(pageSize),
                        foodSearch
                ));
            }
        } catch (SQLException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/restaurants/restaurantSearch", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllRestaurantsWithRestaurantSearch(
            @RequestParam(value = "search", required = false) String restaurantSearch,
            @RequestParam(value = "pageNumber", required = false, defaultValue = "1") String pageNumber,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") String pageSize
    ) {
        try {
            if (restaurantSearch == null || restaurantSearch.isEmpty()) {
                return ResponseEntity.ok().body(RestaurantUtils.getAllRestaurants(
                        Integer.parseInt(pageNumber),
                        Integer.parseInt(pageSize)
                ));
            } else {
                return ResponseEntity.ok().body(RestaurantUtils.searchWithRestaurantName(
                        Integer.parseInt(pageNumber),
                        Integer.parseInt(pageSize),
                        restaurantSearch
                ));
            }
        } catch (SQLException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/restaurant/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getRestaurant(@PathVariable String id) {
        try {
            return ResponseEntity.ok().body(RestaurantUtils.getRestaurantById(id));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        }catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
