package web;

import database.DataBaseAccess;
import models.RestaurantParty;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import util.RestaurantUtils;

import java.awt.*;
import java.sql.SQLException;
import java.util.Collection;

@RestController
public class FoodPartyController {

    @GetMapping(value = "/foodParty", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllRestaurantsInParty() {
        try {
            return ResponseEntity.ok().body(RestaurantUtils.findParties()) ;
        } catch (SQLException ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
