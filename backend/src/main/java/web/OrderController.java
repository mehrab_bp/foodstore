package web;

import database.DataBaseAccess;
import exceptions.*;
import models.FinalizedOrder.FinalizedOrder;
import models.User.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import util.CartUtils;
import util.UserUtils;

import java.sql.SQLException;

@RestController
public class OrderController {

    @GetMapping(value = "/order/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FinalizedOrder> getFinalizedOrder(
            @RequestParam(value = "id") String id,
            @RequestAttribute("user") User user
    ) {
        try {
            FinalizedOrder finalizedOrder = UserUtils.getOrderById(id, user.getId());
            return ResponseEntity.ok().body(finalizedOrder);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (SQLException e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/finalize")
    public ResponseEntity finalizeOrder(
            @RequestAttribute("user") User user
    ) {
        try {
            UserUtils.finalizeOrderForUser(user);
            return new ResponseEntity(HttpStatus.OK);
        } catch (TimeLimitException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMsg());
        } catch (EmptyCartException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMsg());
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        } catch (NotEnoughCreditException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMsg());
        } catch (NotEnoughFoodException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMsg());
        }catch (SQLException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
