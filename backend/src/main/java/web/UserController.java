package web;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import exceptions.DuplicateException;
import exceptions.LoginFailure;
import exceptions.NotFoundException;
import models.User.User;
import models.User.UserMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import util.JWTUtil;
import util.UserUtils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Collections;

@RestController
public class UserController {

    @PostMapping("/user/increaseCredit")
    public ResponseEntity inCreaseCredit(
            @RequestParam(value = "amount") Integer amount,
            @RequestAttribute("user") User user) {
        try {
            return ResponseEntity.ok().body(UserUtils.inCreaseCredit(amount, user.getId()));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        }catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUser(
            @RequestAttribute("user") User user
    ) {
        try {
            return ResponseEntity.ok().body(UserUtils.findUserById(user.getId()));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        }catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/all-users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllUsers() {
        try {
            return ResponseEntity.ok().body(UserUtils.getAllUsers());
        }catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PostMapping(value = "/sign-up")
    public ResponseEntity createNewUser(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "last_name") String last_name,
            @RequestParam(value = "phone_number") String phone_number
    ) {
        try {
            return ResponseEntity.ok().body(UserUtils.createNewUser(email, password, name, last_name, phone_number));
        } catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        } catch (DuplicateException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
    }

    @PostMapping(value = "/login")
    public ResponseEntity makeLogin(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password
    ){
        try {
            return ResponseEntity.ok().body(UserUtils.makeUserLogin(email, password));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMsg());
        }catch (SQLException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        } catch (LoginFailure e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
    }

    private static final String CLIENT_ID = "703688163019-tu8boquvieft42i7akus28qglu8d44n8.apps.googleusercontent.com";

    @PostMapping(value = "/login/google")
    public ResponseEntity loginWithGoogle(
            @RequestParam(value = "idToken") String idToken
    ) {
        try {
            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(
                    new NetHttpTransport.Builder().build(), new JacksonFactory())
                    .setAudience(Collections.singletonList(CLIENT_ID))
                    .build();

            GoogleIdToken token = verifier.verify(idToken);
            if (token != null) {
                GoogleIdToken.Payload payload = token.getPayload();
                String userId = payload.getSubject();
                System.out.println("User ID: " + userId);
                if (payload.getEmailVerified()) {
                    String email = payload.getEmail();
                    try {
                        User user = User.getUserByEmail(email);
                        if(user == null) {
                            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                                    "the user with email: " + email + " is not signedUp!");
                        }
                        String jwt = JWTUtil.createToken(user.getId());
                        return ResponseEntity.ok().body(jwt);
                    } catch (SQLException e) {
                        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("unsuccessful login");
                    }
                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body("some verification went wrong ...");
                }
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("unsuccessful login");
            }
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("google verify problems");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("google verify problems");
        }

    }
}
