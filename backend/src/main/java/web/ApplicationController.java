package web;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import database.DataBaseAccess;
import database.driver.ConnectionPool;
import jobs.FindSomeDeliver;
import jobs.ReducingDeliveryTime;
import jobs.UpdateFoodParty;
import models.FoodInfo.FoodInfo;
import models.FoodInfo.FoodInfoMapper;
import models.FoodParty.FoodParty;
import models.FoodParty.FoodPartyMapper;
import models.Order.OrderMapper;
import models.Restaurant.RestaurantInfo;
import models.Restaurant.RestaurantMapper;
import models.User.UserMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.web.bind.annotation.RestController;
import util.GlobalFunc;
import util.RestaurantUtils;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
public class ApplicationController {

    private static CloseableHttpClient httpClient = HttpClients.createDefault();
    private ScheduledExecutorService scheduler;

    @PostConstruct
    public void initialApplication() {
        try {
            String res = GlobalFunc.SendGetReq("http://138.197.181.131:8080/restaurants", httpClient);
            Set<RestaurantInfo> restaurants = new Gson().fromJson(res, new TypeToken<Set<RestaurantInfo>>(){}.getType());
            DataBaseAccess.getInstance().setRestaurants(restaurants);
            RestaurantUtils.setEstimated();
            initialRestaurants(DataBaseAccess.getInstance().getRestaurants());
            UserMapper.getInstance().setDefaultUser();
            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate(new FindSomeDeliver(), 10, 30, TimeUnit.SECONDS);
            scheduler.scheduleAtFixedRate(new ReducingDeliveryTime(), 5, 10, TimeUnit.SECONDS);
            scheduler.scheduleAtFixedRate(new UpdateFoodParty(), 0, 30, TimeUnit.MINUTES);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void initialRestaurants(Set<RestaurantInfo> restaurantInfos) {
        System.out.println("starting setting rests .******************************");
        try {
            RestaurantMapper mapper = RestaurantMapper.getInstance();
            for (RestaurantInfo restaurantInfo : restaurantInfos) {
                if(restaurantInfo != null){
                    mapper.insert(restaurantInfo);
                    System.out.println(restaurantInfo.getId() + " added");
                    for (FoodInfo food : restaurantInfo.getMenu()) {
                        food.setRestaurantId(restaurantInfo.getId());
                        food.setId(UUID.randomUUID().toString());
                        FoodInfoMapper.getInstance().insert(food);
                    }
                }else{
                    System.out.println("error <-----------------");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("ending seting rests .******************************");
    }
}
