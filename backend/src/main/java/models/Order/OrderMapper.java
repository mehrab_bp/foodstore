package models.Order;

import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.Cart.CartMapper;

import java.sql.*;
import java.util.List;

public class OrderMapper extends Mapper<Order, String> implements BaseOrderMapper{
    private static final String COLUMNS = "id, foodName, price, finalized_id, cart_id, number";
    private static final String TABLE_NAME = "Orders";

    private static OrderMapper instance = null;

    public static OrderMapper getInstance() {
        if (instance == null) {
            try {
                instance = new OrderMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private Boolean doManage;

    public OrderMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE %s " +
                            "(" +
                            "id VARCHAR(100) NOT NULL," +
                            "foodName VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci," +
                            "price INTEGER," +
                            "number INTEGER," +
                            "PRIMARY KEY(id)," +
                            "finalized_id VARCHAR(100)," +
                            "cart_id VARCHAR(100)," +
                            "FOREIGN KEY(finalized_id) REFERENCES FinalizedOrder(id) ON DELETE CASCADE," +
                            "FOREIGN KEY(cart_id) REFERENCES Cart(id) ON DELETE CASCADE" +
                            ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id ='"+ id + "';";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?, ?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, Order order) throws SQLException {
        stmt.setString(1, order.getId());
        stmt.setString(2, order.getFoodName());
        stmt.setString(3, order.getPrice().toString());
        stmt.setString(4, order.getFinalizedId());
        stmt.setString(5, order.getCartId());
        stmt.setString(6, order.getNumber().toString());
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id='" + id + "';";
    }

    @Override
    protected Order convertResultSetToObject(ResultSet rs) throws SQLException {
        return new Order(
                rs.getString(1),
                rs.getString(2),
                rs.getInt(3),
                rs.getString(4),
                rs.getString(5),
                rs.getInt(6)
        );
    }

    @Override
    public List<Order> getOrdersByCartId(String cartId) throws SQLException{
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " WHERE cart_id='" + cartId + "';";

        return executeListReturner(statement);
    }

    @Override
    public void addFinalizedIdToOrder(Order order, String finalizedId) throws SQLException{
        String statement = "UPDATE " + TABLE_NAME +
                " SET finalized_id='" + finalizedId + "'" +
                " WHERE id='" + order.getId() + "';";
        update("SET FOREIGN_KEY_CHECKS = 0");
        update(statement);
        update("SET FOREIGN_KEY_CHECKS = 1");
    }

    @Override
    public void updateCartId(Order order, String cartId) throws SQLException{
        String statement = "UPDATE " + TABLE_NAME +
                " SET cart_id='" + cartId + "'" +
                " WHERE id='" + order.getId() + "';";
        update("SET FOREIGN_KEY_CHECKS = 0");
        update(statement);
        update("SET FOREIGN_KEY_CHECKS = 1");
    }

    @Override
    public void updateOrderNumberAndPrice(Order order, Integer newPrice, Integer newNumberOfOrder) throws SQLException {
        String statement = "UPDATE " + TABLE_NAME +
                " SET number=" + newNumberOfOrder.toString() + ", " + "price=" + newPrice.toString() +
                " WHERE id='" + order.getId() + "';";
        update(statement);
    }

    @Override
    public List<Order> getRelatedOrdersByFinalizeId(String finalizedId) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " WHERE finalized_id='" + finalizedId + "';";
        return executeListReturner(statement);
    }

    @Override
    public void DeleteOrdersByCartID(String cartId) throws SQLException {
        String statement = "DELETE FROM " + TABLE_NAME +
                " WHERE cart_id='" + cartId + "';";
        deleteByStatement(statement);
    }
}

