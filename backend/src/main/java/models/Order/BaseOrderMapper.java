package models.Order;

import database.driver.IMapper;

import java.sql.SQLException;
import java.util.List;

public interface BaseOrderMapper extends IMapper<Order, String> {
    List<Order> getOrdersByCartId(String cartId) throws SQLException;
    void addFinalizedIdToOrder(Order order, String finalizedId) throws SQLException;
    void updateCartId(Order order, String cartId) throws SQLException;
    void updateOrderNumberAndPrice(Order order, Integer newPrice, Integer newNumberOfOrder) throws SQLException;
    List<Order> getRelatedOrdersByFinalizeId(String finalizedId) throws SQLException;
    void DeleteOrdersByCartID(String cartId) throws SQLException;
}
