package models.Order;

import java.sql.SQLException;
import java.util.List;

public class Order {
    private String foodName;
    private Integer number;
    private Integer price;
    private OrderMapper orderMapper;

    public Order(String foodName, Integer number, Integer price) {
        this.foodName = foodName;
        this.number = number;
        this.price = price;
    }

    public Order(String foodName, Integer number) {
        this.foodName = foodName;
        this.number = number;
    }

    public Order(String id, String foodName, int price, String finalizedId, String cartId, Integer number) {
        this.id = id;
        this.foodName=foodName;
        this.price=price;
        this.finalizedId=finalizedId;
        this.cartId=cartId;
        this.number = number;
        this.orderMapper = OrderMapper.getInstance();
    }

    public Order(String id, String foodName, int price, String cartId, Integer number){
        this.id = id;
        this.foodName=foodName;
        this.price=price;
        this.cartId=cartId;
        this.number = number;
        this.orderMapper = OrderMapper.getInstance();
    }

    private Order getUpdatedOrder() throws SQLException {
            return orderMapper.find(this.id);
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    private String id;
    public String getId(){return this.id;}
    public void setId(String id){this.id = id;}

    private String finalizedId;
    public String getFinalizedId(){return this.finalizedId;}
    public void setFinalizedId(String finalizedId) throws SQLException {
        orderMapper.addFinalizedIdToOrder(this, finalizedId);
        this.finalizedId=finalizedId;
    }

    private String cartId;
    public String getCartId(){return  this.cartId;}
    public void setCartId(String cartId) throws SQLException {
        orderMapper.updateCartId(this, cartId);
        this.cartId=cartId;
    }

    public void incrementNumber() throws SQLException {
        this.price = (this.price/this.number) + this.price;
        this.number = this.number + 1;
        orderMapper.updateOrderNumberAndPrice(this, this.price, this.number);
    }

    public void decrementNumber() throws SQLException{
        this.price = this.price - (this.price/this.number);
        this.number = this.number - 1;
        if(this.number == 0){
            orderMapper.delete(this.id);
        }else{
            orderMapper.updateOrderNumberAndPrice(this, this.price, this.number);
        }
    }

    public static List<Order> findOrdersByCartId(String cartId) throws SQLException {
        return OrderMapper.getInstance().getOrdersByCartId(cartId);
    }

    public static List<Order> findOrdersByFinalizedId(String finalizedId) throws SQLException {
        return OrderMapper.getInstance().getRelatedOrdersByFinalizeId(finalizedId);
    }

    public void saveOrder() throws SQLException{
        orderMapper.insert(this);
    }
}
