package models;

public class Delivery {
    String id;
    Integer velocity;
    Location location;

    public Delivery(String id, Integer velocity, Location location) {
        this.id = id;
        this.velocity = velocity;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVelocity() {
        return velocity;
    }

    public void setVelocity(Integer velocity) {
        this.velocity = velocity;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
