package models;

public class Time {
    private Integer min;
    private Integer sec;

    public Time(Integer min, Integer sec) {
        this.min = min;
        this.sec = sec;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getSec() {
        return sec;
    }

    public void setSec(Integer sec) {
        this.sec = sec;
    }

    public Time(String time){
        String[] timesPart = time.split(":");
        if(timesPart.length == 2){
            this.min = Integer.parseInt(timesPart[0]);
            this.sec = Integer.parseInt(timesPart[1]);
        }else{
            System.out.println("there is some error with casting");
            this.min = 0;
            this.sec = 0;
        }
    }

    public boolean hasFinished() {
        if (min <= 0 && sec <= 0)
            return true;
        return false;
    }

    public void decreaseTenSec() {
        if (sec <= 10 && min > 0) {
            min -= 1;
            sec = 50;
        } else if (sec <= 10) {
            min = 0;
            sec = 0;
        } else {
            sec -= 10;
        }
    }

    @Override
    public String toString() {
        return min + ":" + sec;
    }
}
