package models.FoodInfo;

import database.driver.IMapper;

import java.sql.SQLException;
import java.util.List;

public interface FoodInfoBaseMapper extends IMapper<FoodInfo, String> {
    List<FoodInfo> getFoodParties() throws SQLException;
    List<FoodInfo> getFoodsByRestaurantId(String restaurantId) throws SQLException;
    FoodInfo getSingleOneByRestaurantId(String restaurantId, String id) throws SQLException;
}
