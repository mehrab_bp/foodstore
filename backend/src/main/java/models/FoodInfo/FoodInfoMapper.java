package models.FoodInfo;

import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.FinalizedOrder.FinalizedOrderMapper;

import java.sql.*;
import java.util.List;

public class FoodInfoMapper extends Mapper<FoodInfo, String> implements FoodInfoBaseMapper {
    private static final String COLUMNS = "id, name, description, popularity, price, image, restaurant_id, is_food_party";
    private static final String TABLE_NAME = "Food";

    private Boolean doManage;

    private static FoodInfoMapper instance = null;

    public static FoodInfoMapper getInstance() {
        if (instance == null) {
            try {
                instance = new FoodInfoMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public FoodInfoMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE  %s " +
                            "(" +
                            "id VARCHAR(100) NOT NULL," +
                            "PRIMARY KEY(id)," +
                            "name VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci," +
                            "description VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci," +
                            "popularity float," +
                            "price INTEGER," +
                            "image VARCHAR(250)," +
                            "restaurant_id VARCHAR(100)," +
                            "FOREIGN KEY(restaurant_id) REFERENCES Restaurant(id) ON DELETE CASCADE," +
                            "is_food_party VARCHAR(100)" +
                            ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id='"+ id + "';";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, FoodInfo foodInfo) throws SQLException {
        stmt.setString(1, foodInfo.getId());
        stmt.setString(2, foodInfo.getName());
        stmt.setString(3, foodInfo.getDescription());
        stmt.setFloat(4, foodInfo.getPopularity().floatValue());
        stmt.setInt(5, foodInfo.getPrice());
        stmt.setString(6, foodInfo.getImage());
        stmt.setString(7, foodInfo.getRestaurantId());
        stmt.setBoolean(8, foodInfo.isFoodParty());
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id='" + id + "';";
    }

    @Override
    protected FoodInfo convertResultSetToObject(ResultSet rs) throws SQLException {
        return new FoodInfo(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getDouble(4),
                rs.getInt(5),
                rs.getString(6),
                rs.getString(7)
        );
    }

    @Override
    public List<FoodInfo> getFoodParties() throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where is_food_party=true;";
        return executeListReturner(statement);
    }

    @Override
    public List<FoodInfo> getFoodsByRestaurantId(String restaurantId) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where restaurant_id='" + restaurantId + "';" ;
        return executeListReturner(statement);
    }

    @Override
    public FoodInfo getSingleOneByRestaurantId(String restaurantId, String id) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where restaurant_id='" + restaurantId + "' and id='" + id + "';" ;
        return executeSingleReturner(statement);
    }
}
