package models.FoodInfo;

import com.google.gson.Gson;

import java.sql.SQLException;

public class FoodInfo {
    private String id;
    private String name;
    private String description;
    private Double popularity;
    private Integer price;
    private String image;
    private String restaurantId;
    private boolean isFoodParty = false;

    public FoodInfo(String name, String description, Double popularity, Integer price) {
        this.name = name;
        this.description = description;
        this.popularity = popularity;
        this.price = price;
    }

    public FoodInfo(String id, String name, String description, Double popularity, Integer price, String image, String restaurantId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.popularity = popularity;
        this.price = price;
        this.image = image;
        this.restaurantId=restaurantId;
    }

    public String getId(){return this.id;}

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getRestaurantId(){return this.restaurantId;}

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public boolean isFoodParty() {
        return isFoodParty;
    }

    public void setFoodParty(boolean foodParty) {
        isFoodParty = foodParty;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static FoodInfo getSingleOneByRestaurantId(String restaurantId, String id) throws SQLException {
        return FoodInfoMapper.getInstance().getSingleOneByRestaurantId(restaurantId, id);
    }
}
