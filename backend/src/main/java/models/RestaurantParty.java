package models;

import models.FoodParty.FoodParty;

import java.util.List;

public class RestaurantParty {
    private String id;
    private String name;
    private Location location;
    private String logo;
    private List<FoodParty> menu;

    public RestaurantParty(String id, String name, Location location, String logo, List<FoodParty> menu) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.logo = logo;
        this.menu = menu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<FoodParty> getMenu() {
        return menu;
    }

    public void setMenu(List<FoodParty> menu) {
        this.menu = menu;
    }
}
