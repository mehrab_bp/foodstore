package models.FoodParty;

import database.driver.IMapper;
import models.FoodParty.FoodParty;

import java.sql.SQLException;
import java.util.List;

public interface FoodPartyBaseMapper extends IMapper<FoodParty, String> {
    List<FoodParty> getAllData() throws SQLException;
    void deleteAllData() throws SQLException;
    void updateCount(String id, Integer newCount) throws SQLException;
    List<FoodParty> getFoodPartiesByRestaurantId(String restaurantId) throws SQLException;
}
