package models.FoodParty;

import models.FoodInfo.FoodInfo;

public class FoodParty extends FoodInfo {
    private Integer oldPrice;
    private Integer count;

    public FoodParty(String id, String restaurantId, String name, String description, Double popularity, Integer price, String image, Integer oldPrice, Integer count) {
        super(id, name, description, popularity, price, image, restaurantId);
        this.oldPrice = oldPrice;
        this.count = count;
    }

    public Integer getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Integer oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
