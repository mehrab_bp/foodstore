package models.FoodParty;

import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.FoodInfo.FoodInfo;
import models.FoodInfo.FoodInfoMapper;

import java.sql.*;
import java.util.List;

public class FoodPartyMapper extends Mapper<FoodParty, String> implements FoodPartyBaseMapper {
    private static final String COLUMNS = "foodId, count, oldPrice";
    private static final String TABLE_NAME = "foodParty";

    private Boolean doManage;

    private static FoodPartyMapper instance = null;

    public static FoodPartyMapper getInstance() {
        if (instance == null) {
            try {
                instance = new FoodPartyMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public FoodPartyMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE  %s " +
                            "(" +
                            "foodId VARCHAR(100) NOT NULL," +
                            "PRIMARY KEY(foodId)," +
                            "FOREIGN KEY(foodId) REFERENCES Food(id) ON DELETE CASCADE," +
                            "count INTEGER," +
                            "oldPrice INTEGER" +
                            ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        String statement = "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id='"+ id + "';";
        return statement;
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, FoodParty foodParty) throws SQLException {
        stmt.setString(1, foodParty.getId());
        stmt.setInt(2, foodParty.getCount());
        stmt.setInt(3, foodParty.getOldPrice());
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id = " + id + ";";
    }

    @Override
    protected FoodParty convertResultSetToObject(ResultSet rs) throws SQLException {
        FoodInfo foodInfo = FoodInfoMapper.getInstance().find(rs.getString(1));
        return new FoodParty(
                rs.getString(1),
                foodInfo.getRestaurantId(),
                foodInfo.getName(),
                foodInfo.getDescription(),
                foodInfo.getPopularity(),
                foodInfo.getPrice(),
                foodInfo.getImage(),
                rs.getInt(3),
                rs.getInt(2)
        );
    }

    @Override
    public List<FoodParty> getFoodPartiesByRestaurantId(String restaurantId) throws SQLException{
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME + " " +
                "INNER JOIN Food ON foodParty.foodId=Food.id " +
                "Where Food.restaurant_id='" + restaurantId + "';" ;
        return executeListReturner(statement);
    }

    @Override
    public void updateCount(String id, Integer newCount) throws SQLException{
        String statement = "UPDATE " + TABLE_NAME + " " +
                "SET count='" + newCount.toString() + "' " +
                "WHERE foodId='" + id + "';";
        update(statement);
    }

    @Override
    public void deleteAllData() throws SQLException {
        String statement = "DELETE FROM " + TABLE_NAME + ";";
        deleteByStatement(statement);
    }

    @Override
    public List<FoodParty> getAllData() throws SQLException {
        String statement = "SELECT * FROM " + TABLE_NAME + ";";
        return executeListReturner(statement);
    }
}
