package models.User;

import database.driver.IMapper;

import java.sql.SQLException;
import java.util.List;

public interface UserMapperBase extends IMapper<User, String> {
    List<User> getContainsText(String text) throws SQLException;
    List<User> getAllUsers() throws SQLException;
    User getUserByEmail(String email) throws SQLException;
    void updateCredit(String id, Integer newCredit) throws SQLException;
    void setDefaultUser() throws SQLException;
    User getDefaultUser() throws SQLException;
}