package models.User;
import com.google.gson.Gson;
//import jdk.internal.jline.internal.Nullable;
import lombok.Data;
import lombok.NonNull;
import models.Cart.Cart;
import models.FinalizedOrder.FinalizedOrder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {
    @NonNull private String id;
    @NonNull private String name;
    @NonNull private String lastName;
    @NonNull private String phoneNumber;
    @NonNull private String email;
    @NonNull private Integer credit;
    @NonNull private String password;
    private List<FinalizedOrder> finalizedOrders;
    private Cart cart;

//    public User(String id) throws SQLException {
//        this.id = id;
//        this.name = "احسان";
//        this.lastName = "خامس پناه";
//        this.phoneNumber = "09124820194";
//        this.email = "ekhamespanah@yahoo.com";
//        this.credit = 100000;
//        this.saveThis();
//        this.cart = new Cart(this.id);
//    }

    public User(String id, String name, String lastName, String phoneNumber, String email, Integer credit, String password) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.credit = credit;
        this.password = password;
    }

    public User(String id, String name, String lastName, String phoneNumber, String email, Integer credit, String password,
                Cart cart, List<FinalizedOrder> finalizedOrders) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.credit = credit;
        this.password = password;
        this.cart = cart;
        this.finalizedOrders = finalizedOrders;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<FinalizedOrder> getFinalizedOrders() throws SQLException {
        return finalizedOrders;
    }

    public void setFinalizedOrders(List<FinalizedOrder> finalizedOrders) {
        this.finalizedOrders = finalizedOrders;
    }

    public void addFinalizeOrder(FinalizedOrder finalizedOrder) throws SQLException {
        this.finalizedOrders.add(finalizedOrder);
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public Integer getCredit(){
        return credit;
    }

    public String getId(){return id;}

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCredit(Integer credit) throws SQLException {
        this.credit = credit;
    }

    public Cart getCart() throws SQLException {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public void saveThis() throws SQLException {
        UserMapper.getInstance().insert(this);
    }

    public static User getUserById(String id) throws SQLException{
        return UserMapper.getInstance().find(id);
    }

    public static User getUserByEmail(String email) throws SQLException {
        return UserMapper.getInstance().getUserByEmail(email);
    }

    public static List<User>getAllUsers()throws SQLException{
        return UserMapper.getInstance().getAllUsers();
    }

    public void setCreditToDb() throws SQLException {
        UserMapper.getInstance().updateCredit(this.id, this.credit);
    }

    public String getPassword(){return this.password;}

    public String toJson() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }
}