package models.User;

import com.google.common.hash.Hashing;
import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.Cart.Cart;
import models.Cart.CartMapper;
import models.FinalizedOrder.FinalizedOrder;
import models.FinalizedOrder.FinalizedOrderMapper;
import models.Order.Order;
import models.Order.OrderMapper;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserMapper extends Mapper<User, String> implements UserMapperBase {

    private static final String COLUMNS = " id, name, lastName, phoneNumber, email, credit, password";
    private static final String TABLE_NAME = "User";

    private Boolean doManage;

    private static UserMapper instance = null;

    public static UserMapper getInstance() {
        if (instance == null) {
            try {
                instance = new UserMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public UserMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE  %s " +
                    "(" +
                            "id VARCHAR(100) PRIMARY KEY, " +
                            "name VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci, " +
                            "lastName VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci, " +
                            "phoneNumber VARCHAR(100), " +
                            "email VARCHAR(150) UNIQUE, " +
                            "credit integer, " +
                            "password VARCHAR(1000) " +
                    ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id='"+ id + "';";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, User user) throws SQLException {
        stmt.setString(1, user.getId());
        stmt.setString(2, user.getName());
        stmt.setString(3, user.getLastName());
        stmt.setString(4, user.getPhoneNumber());
        stmt.setString(5, user.getEmail());
        stmt.setString(6, user.getCredit().toString());
        stmt.setString(7, user.getPassword());
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id='" + id+ "';";
    }

    @Override
    protected User convertResultSetToObject(ResultSet rs) throws SQLException {
        List<FinalizedOrder> finalizedOrders = FinalizedOrderMapper.getInstance().getFinalizedByUserId(rs.getString(1));
        Cart cart = CartMapper.getInstance().getCartByUserId(rs.getString(1));
        return  new User(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getInt(6),
                rs.getString(7),
                cart,
                finalizedOrders
        );
    }

    @Override
    public List<User> getContainsText(String text) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where name LIKE " + "'%" + text + "%'";
        return executeListReturner(statement);
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME + ";";
        return executeListReturner(statement);
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        String statement = "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE email='"+ email + "';";
        return executeSingleReturner(statement);
    }

    @Override
    public void updateCredit(String id, Integer newCredit) throws SQLException {
        String statement = "UPDATE " + TABLE_NAME +
                " SET credit=" + newCredit.toString() +
                " WHERE id='" + id + "';";
        update(statement);
    }

    @Override
    public void setDefaultUser() throws SQLException {
        String userId = "1";
        String name = "مهراب";
        String lastName = "بخشی پور";
        String phoneNumber = "0938 390 3778";
        String email = "mbpmbp1998@gmail.com";
        Integer credit = 100000;
        String password = Hashing.sha256().hashString("pass", StandardCharsets.UTF_8).toString();
        User user1 = new User(userId, name, lastName, phoneNumber, email, credit, password);
        UserMapper.getInstance().insert(user1);
        Cart cart1 = new Cart(UUID.randomUUID().toString(), "", "", userId);
        CartMapper.getInstance().insert(cart1);

        userId = "2";
        name = "جواد";
        lastName = "بیاتی";
        phoneNumber = "0910 967 9322";
        email = "m.javad.bayati97@gmail.com";
        credit = 100001;
        User user2 = new User(userId, name, lastName, phoneNumber, email, credit, password);
        UserMapper.getInstance().insert(user2);
        Cart cart2 = new Cart(UUID.randomUUID().toString(), "", "", userId);
        CartMapper.getInstance().insert(cart2);
    }

    @Override
    public User getDefaultUser() throws SQLException {
        User defaultUser = UserMapper.getInstance().find("2");
        return defaultUser;
    }
}
