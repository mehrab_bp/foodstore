package models.Cart;

import database.driver.IMapper;

import java.sql.SQLException;

public interface CartMapperBase extends IMapper<Cart, String> {
    Cart getCartByUserId(String userId) throws SQLException;
    void updateRestaurant(Cart cart, String restaurantName, String restaurantId) throws SQLException;
}
