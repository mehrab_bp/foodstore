package models.Cart;

import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.Order.Order;
import models.Order.OrderMapper;
import models.User.User;

import java.sql.*;
import java.util.List;

public class CartMapper extends Mapper<Cart, String> implements CartMapperBase{
    private static final String COLUMNS = "id, restaurantName, restaurantId, user_id";
    private static final String TABLE_NAME = "Cart";
    private Boolean doManage;

    private static CartMapper instance = null;

    public static CartMapper getInstance() {
        if (instance == null) {
            try {
                instance = new CartMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }


    public CartMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE  %s " +
                            "(" +
                            "id VARCHAR(100) NOT NULL," +
                            "restaurantId VARCHAR(100), " +
                            "restaurantName VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci, " +
                            "PRIMARY KEY(id)," +
                            "user_id VARCHAR(100)," +
                            "FOREIGN KEY(user_id) REFERENCES User(id) ON DELETE CASCADE" +
                            ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id ='"+ id + "';";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, Cart cart) throws SQLException {
        stmt.setString(1, cart.getId());
        stmt.setString(2, cart.getRestaurantName());
        stmt.setString(3, cart.getRestaurantId());
        stmt.setString(4, cart.getUserId());
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id ='" + id + "';";
    }

    @Override
    protected Cart convertResultSetToObject(ResultSet rs) throws SQLException {
        List<Order> orders = OrderMapper.getInstance().getOrdersByCartId(rs.getString(1));
        return  new Cart(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                orders
        );
    }

    @Override
    public Cart getCartByUserId(String userId) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where user_id='" + userId + "';";
        return executeSingleReturner(statement);
    }

    @Override
    public void updateRestaurant(Cart cart, String restaurantName, String restaurantId) throws SQLException {
        String statement = "UPDATE " + TABLE_NAME +
                " SET restaurantName='" + restaurantName + "', " +  "restaurantId='" + restaurantId + "'" +
                " WHERE id='" + cart.getId() + "';";
        update(statement);
    }
}
