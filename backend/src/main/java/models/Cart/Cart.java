package models.Cart;

import models.Order.Order;
import models.Order.OrderMapper;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class Cart {
    private List<Order> orders;
    private String restaurantName;
    private String restaurantId;
    private String id;
    private String userId;

    public Cart(String id, String restaurantName, String restaurantId, String userId) {
        this.id = id;
        this.restaurantName = restaurantName;
        this.restaurantId = restaurantId;
        this.userId = userId;
    }

    public Cart(String id, String restaurantName, String restaurantId, String userId, List<Order> orders) {
        this.id = id;
        this.restaurantName = restaurantName;
        this.restaurantId = restaurantId;
        this.userId = userId;
        this.orders = orders;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Cart(String userId) throws SQLException {
        this.userId = userId;
        this.id = UUID.randomUUID().toString();
        this.saveThis();
    }

    public List<Order> getOrders() throws SQLException {
        return Order.findOrdersByCartId(this.id);
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public void addToOrder(String foodName, Integer price) throws SQLException {
        List<Order> orders = this.getOrders();
        boolean orderExistence = false;
        for (Order order: orders) {
            if (order.getFoodName().equals(foodName)) {
                order.incrementNumber();
                orderExistence=true;
            }
        }
        if (!orderExistence) {
            String orderId = UUID.randomUUID().toString();
            Order order = new Order(orderId, foodName, price, this.id, 1); //todo id's should be fixed
            order.saveOrder();
        }
    }

    public void reduceFromOrder(String foodName) throws SQLException {
        for (Order order: this.getOrders()) {
            if (order.getFoodName().equals(foodName)) {
                order.decrementNumber();
            }
        }
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

//    public void setUserId(String userId){this.userId = userId;}

    public String getUserId(){return this.userId;}

    public void clear() {
        orders.clear();
        restaurantName = "";
        restaurantId = "";
    }

    public static Cart findByUserId(String id) throws SQLException {
        return CartMapper.getInstance().getCartByUserId(id);
    }

    public static Cart getCartByItsOrdersFromDBByUserId(String userId) throws SQLException {
        Cart cart = Cart.findByUserId(userId);
        if(cart == null){
            return null;
        }
        cart.setOrders(OrderMapper.getInstance().getOrdersByCartId(cart.id));
        return cart;
    }

    public void saveThis() throws SQLException {
        CartMapper.getInstance().insert(this);
    }

    public void updateRestaurantInDB(String restaurantName, String restaurantId) throws SQLException {
        CartMapper.getInstance().updateRestaurant(this, restaurantName, restaurantId);
    }

    public void deleteCartOrdersFromDB() throws SQLException {
        OrderMapper.getInstance().DeleteOrdersByCartID(this.id);
    }

    public void detachOrdersFromCart() throws SQLException{
        List<Order> orders = OrderMapper.getInstance().getOrdersByCartId(this.id);
        for(Order order: orders){
            OrderMapper.getInstance().updateCartId(order, null);
        }
    }
}
