package models.FinalizedOrder;

import com.github.mfathi91.time.PersianDate;
import models.Cart.Cart;
import models.Cart.CartMapper;
import models.Order.Order;
import models.Time;
import util.GlobalFunc;

import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

public class FinalizedOrder {
    private String id;
    private List<Order> orders;
    private String restaurantName;
    private String restaurantId;
    private Time deliveryTime;
    private DeliveryStatus status;
    private PersianDate date;
    private String userId;


    public FinalizedOrder(String id) throws SQLException {
        this.id = id;
    }

    public FinalizedOrder(String id, String restaurantName, String restaurantId, java.sql.Time deliveryTime, int status, Long persianDate, String userId) {
        this.id = id;
        this.restaurantName = restaurantName;
        this.restaurantId = restaurantId;
        this.deliveryTime = GlobalFunc.convertTime(deliveryTime);
        this.status = DeliveryStatus.values()[status];
        this.date = PersianDate.ofEpochDay(persianDate);
        this.userId = userId;
    }

    public FinalizedOrder(String id, String restaurantName, String restaurantId, String deliveryTime, int status, Long persianDate, String userId, List<Order> orders) {
        this.id = id;
        this.restaurantName = restaurantName;
        this.restaurantId = restaurantId;
        this.deliveryTime = new Time(deliveryTime);
        this.status = DeliveryStatus.values()[status];
        this.date = PersianDate.ofEpochDay(persianDate);
        this.userId = userId;
        this.orders = orders;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Order> getOrders() throws SQLException {
        return Order.findOrdersByFinalizedId(this.id);
    }

    public void setOrders(List<Order> orders) throws SQLException {
        this.orders = orders;
    }

    public void addOrdersInDB (List<Order> orders) throws SQLException {
        for(Order order : orders) {
            order.setFinalizedId(this.id);
        }
    }

    public String getUserId(){return this.userId;}

    public void setUserId(String userId){this.userId=userId;}

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Time getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Time deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public PersianDate getDate() {
        return date;
    }

    public void setDate(PersianDate date) {
        this.date = date;
    }

    public DeliveryStatus getStatus() {
        return status;
    }

    public void setStatus(DeliveryStatus status) {
        this.status = status;
    }

    public enum DeliveryStatus {
        findingDelivery,
        delivering,
        done;
    }

    public static Integer getIntValueOfDeliveryStatus(DeliveryStatus deliveryStatus){
        if(deliveryStatus == DeliveryStatus.findingDelivery){
            return 0;
        }else if(deliveryStatus == DeliveryStatus.delivering){
            return 1;
        }else{
            return 2;
        }
    }

    public static FinalizedOrder findById(String id) throws SQLException{
        return FinalizedOrderMapper.getInstance().find(id);
    }

    public static FinalizedOrder findSingleByUserId(String userId, String id) throws SQLException {
        return FinalizedOrderMapper.getInstance().getSingleOneByUserId(userId, id);
    }

    public static List<FinalizedOrder> findByUserId(String id) throws SQLException {
        return FinalizedOrderMapper.getInstance().getFinalizedByUserId(id);
    }

    public void saveThis() throws SQLException {
        FinalizedOrderMapper.getInstance().insert(this);
    }
}
