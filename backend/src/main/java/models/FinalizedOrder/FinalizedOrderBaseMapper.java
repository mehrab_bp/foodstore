package models.FinalizedOrder;

import database.driver.IMapper;

import java.sql.SQLException;
import java.util.List;

public interface FinalizedOrderBaseMapper  extends IMapper<FinalizedOrder, String> {
    void updateStatus(String id, Integer status, String time) throws SQLException;
    List<FinalizedOrder> getFinalizedByUserId(String userId) throws SQLException;
    FinalizedOrder getSingleOneByUserId(String userId, String id) throws SQLException;
}
