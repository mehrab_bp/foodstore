package models.FinalizedOrder;

import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.Cart.Cart;
import models.Cart.CartMapper;
import models.Order.Order;
import models.Order.OrderMapper;

import java.sql.*;
import java.util.List;

public class FinalizedOrderMapper extends Mapper<FinalizedOrder, String> implements FinalizedOrderBaseMapper {
    private static final String COLUMNS = "id, restaurantName, restaurantId, deliveryTime, status, date, user_id";
    private static final String TABLE_NAME = "FinalizedOrder";

    private Boolean doManage;

    private static FinalizedOrderMapper instance = null;

    public static FinalizedOrderMapper getInstance() {
        if (instance == null) {
            try {
                instance = new FinalizedOrderMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public FinalizedOrderMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE  %s " +
                            "(" +
                            "id VARCHAR(100) NOT NULL, " +
                            "restaurantName VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci, " +
                            "restaurantId VARCHAR(100)," +
                            "deliveryTime VARCHAR(100)," +
                            "status INTEGER," +
                            "date VARCHAR(100)," +
                            "user_id VARCHAR(100)," +
                            "PRIMARY KEY(id)," +
                            "FOREIGN KEY(restaurantId) REFERENCES Restaurant(id) ON DELETE CASCADE," + //todo set this with mehrab
                            "FOREIGN KEY(user_id) REFERENCES User(id) ON DELETE CASCADE" +
                            ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id='"+ id + "';";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, FinalizedOrder finalizedOrder) throws SQLException {
        Integer status = FinalizedOrder.getIntValueOfDeliveryStatus(finalizedOrder.getStatus());
        stmt.setString(1, finalizedOrder.getId());
        stmt.setString(2, finalizedOrder.getRestaurantName());
        stmt.setString(3, finalizedOrder.getRestaurantId());
        stmt.setString(4, finalizedOrder.getDeliveryTime().toString());
        stmt.setString(5, Integer.toString(finalizedOrder.getStatus().ordinal()));
        stmt.setString(6, Long.toString(finalizedOrder.getDate().toEpochDay()));
        stmt.setString(7, finalizedOrder.getUserId());
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id='" + id + "';";
    }

    @Override
    protected FinalizedOrder convertResultSetToObject(ResultSet rs) throws SQLException {
        List<Order> orders = OrderMapper.getInstance().getRelatedOrdersByFinalizeId(
                rs.getString(1));
        return new FinalizedOrder(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getInt(5),
                rs.getLong(6),
                rs.getString(7),
                orders
        );
    }

    @Override
    public void updateStatus(String id, Integer status, String time) throws SQLException {
        String statement = "UPDATE " + TABLE_NAME +
                " SET status=" + status.toString() + ", " + "deliveryTime='" + time + "'" +
                " WHERE id='" + id + "';";
        update(statement);
    }

    @Override
    public List<FinalizedOrder> getFinalizedByUserId(String userId) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where user_id='" + userId + "';";
        return executeListReturner(statement);
    }

    @Override
    public FinalizedOrder getSingleOneByUserId(String userId, String id) throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where user_id='" + userId + "' and id='" + id + "';";
        return executeSingleReturner(statement);
    }
}
