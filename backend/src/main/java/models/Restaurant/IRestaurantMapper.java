package models.Restaurant;

import database.driver.IMapper;
import models.Restaurant.RestaurantInfo;

import java.sql.SQLException;
import java.util.List;

public interface IRestaurantMapper extends IMapper<RestaurantInfo, String> {

    List<RestaurantInfo> getAllRestaurants(Integer pageNumber, Integer pageSize) throws SQLException;
    List<RestaurantInfo> getSearchedRestaurantsByName(Integer pageNumber, Integer pageSize, String search)
            throws SQLException;
    List<RestaurantInfo> getSearchedRestaurantsByFoodName(Integer pageNumber, Integer pageSize, String search)
            throws SQLException;
    void updateFoodParty(String id, boolean isFoodParty) throws SQLException;
    void updateEstimatedDeliveryTime(RestaurantInfo restaurantInfo, String estimatedDelivery) throws SQLException;
    List<RestaurantInfo> getParties() throws SQLException;
}
