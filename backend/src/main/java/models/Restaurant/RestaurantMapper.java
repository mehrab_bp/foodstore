package models.Restaurant;

import database.driver.ConnectionPool;
import database.driver.Mapper;
import models.FoodInfo.FoodInfo;
import models.FoodInfo.FoodInfoMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RestaurantMapper extends Mapper<RestaurantInfo, String> implements IRestaurantMapper {
    private static final String TABLE_NAME = "Restaurant";
    private static final String COLUMNS = "Restaurant.id, Restaurant.name, location, logo, estimated_delivery, isFoodParty";
    private static RestaurantMapper instance = null;

    public static RestaurantMapper getInstance() {
        if (instance == null) {
            try {
                instance = new RestaurantMapper(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private Boolean doManage;

    public RestaurantMapper(Boolean doManage) throws SQLException {
        if (this.doManage = doManage) {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            try{
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
            }catch (SQLException e){
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
                st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                st.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            }
            st.executeUpdate(String.format(
                    "CREATE TABLE  %s " +
                            "(" +
                            "id VARCHAR(100) PRIMARY KEY, " +
                            "name VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci," +
                            "location VARCHAR(100)," +
                            "logo VARCHAR(200)," +
                            "estimated_delivery VARCHAR(100)," +
                            "isFoodParty VARCHAR(100)" +
                            ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
                    TABLE_NAME));
            System.out.println(TABLE_NAME + "Created!");
            st.close();
            con.close();
        }
    }

    @Override
    protected String getFindStatement(String id) {
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id='"+ id + "';";
    }

    @Override
    protected String getInsertStatement() {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" +
                "VALUES(?, ?, ?, ?, ?, ?);";
    }

    @Override
    public void fillInsertStatement(PreparedStatement stmt, RestaurantInfo restaurantInfo) throws SQLException {
        stmt.setString(1, restaurantInfo.getId());
        stmt.setString(2, restaurantInfo.getName());
        stmt.setString(3, restaurantInfo.getLocation().toString());
        stmt.setString(4, restaurantInfo.getLogo());
        stmt.setString(5, restaurantInfo.getEstimatedDelivery().toString());
        stmt.setBoolean(6, restaurantInfo.isFoodParty());
    }


    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME +
                " WHERE id='" + id + "';";
    }

    @Override
    protected RestaurantInfo convertResultSetToObject(ResultSet rs) throws SQLException {
        return new RestaurantInfo(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getBoolean(6)
        );
    }

    @Override
    public List<RestaurantInfo> getAllRestaurants(Integer pageNumber, Integer pageSize) throws SQLException {
        String statement = "SELECT "+ COLUMNS + " FROM " + TABLE_NAME + " " +
                "WHERE isFoodParty=false ";
        return findListWithPagination(statement , pageNumber, pageSize);
    }

    @Override
    public List<RestaurantInfo> getSearchedRestaurantsByName(Integer pageNumber, Integer pageSize, String search) throws SQLException {
        String statement = "SELECT "+ COLUMNS + " FROM " + TABLE_NAME + " " +
                "WHERE name LIKE " + "'%" + search + "%' ";
        return findListWithPagination(statement , pageNumber, pageSize);
    }

    @Override
    public List<RestaurantInfo> getSearchedRestaurantsByFoodName(Integer pageNumber, Integer pageSize, String search) throws SQLException {
        String statement = "SELECT DISTINCT " + COLUMNS + " FROM " + TABLE_NAME + " " +
                "INNER JOIN Food ON Restaurant.id=Food.restaurant_id " +
                "where Food.name LIKE " + "'%" + search + "%' ";
        return findListWithPagination(statement , pageNumber, pageSize);
    }

    @Override
    public void updateEstimatedDeliveryTime(RestaurantInfo restaurantInfo, String estimatedDelivery) throws SQLException{
        String statement = "UPDATE " + TABLE_NAME +
                " SET estimated_delivery=" + estimatedDelivery +
                " WHERE id='" + restaurantInfo.getId() + "';";
        update(statement);
    }

    @Override
    public void updateFoodParty(String id, boolean isFoodParty) throws SQLException {
        String statement = "UPDATE " + TABLE_NAME +
                " SET isFoodParty=" + isFoodParty +
                " WHERE id='" + id + "';";
        update(statement);
    }

    @Override
    public List<RestaurantInfo> getParties() throws SQLException {
        String statement = "SELECT " + COLUMNS + " FROM " + TABLE_NAME +
                " Where isFoodParty=true;";
        return executeListReturner(statement);
    }
}
