package models.Restaurant;

import com.google.gson.Gson;
import exceptions.NotFoundException;
import lombok.NonNull;
import models.FoodInfo.FoodInfo;
import models.FoodInfo.FoodInfoMapper;
import models.Location;
import models.Time;
import util.GlobalFunc;

import java.sql.SQLException;
import java.util.List;

public class RestaurantInfo {
    @NonNull private String id;
    private String name;
    private Location location;
    private String logo;
    private List<FoodInfo> menu;
    private Time estimatedDelivery;
    private boolean isFoodParty = false;

    public RestaurantInfo(String id, String name, String location, String logo, List<FoodInfo> menu,
                          String estimatedDelivery) {
        this.id = id;
        this.name = name;
        this.location = GlobalFunc.convertToLocation(location);
        this.logo = logo;
        this.menu = menu;
        this.estimatedDelivery = GlobalFunc.convertToTime(estimatedDelivery);
    }

    public RestaurantInfo(String id, String name, String location, String logo, String estimatedDelivery,
                          Boolean isFoodParty) {
        this.id = id;
        this.name = name;
        this.location = GlobalFunc.convertToLocation(location);
        this.logo = logo;
        this.estimatedDelivery = GlobalFunc.convertToTime(estimatedDelivery);
        this.isFoodParty = isFoodParty;

    }

    public RestaurantInfo(String id, String name, Location location, String logo) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public List<FoodInfo> getMenu() throws SQLException {
        return menu;
//        return foodInfoMapper.getFoodsByRestaurantId(this.id);
    }

    public boolean isFoodParty() {
        return isFoodParty;
    }

    public void setFoodParty(boolean foodParty) {
        isFoodParty = foodParty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

//    public void setMenu(List<FoodInfo> menu) {
//        this.menu = menu;
//    }

//    public void addFood(FoodInfo foodInfo) {
//        this.menu.add(foodInfo);
//    }

    public Time getEstimatedDelivery() {
        return estimatedDelivery;
    }

    public void setEstimatedDelivery(Time estimatedDelivery) {
        this.estimatedDelivery = estimatedDelivery;
    }

    public void setMenuFromDB() throws SQLException {
        this.menu = FoodInfoMapper.getInstance().getFoodsByRestaurantId(this.id);
    }

    public static List<RestaurantInfo> fillFoodsForRestaurants(List<RestaurantInfo> restaurantInfos) throws SQLException {
        for(RestaurantInfo restaurantInfo: restaurantInfos){
            restaurantInfo.setMenuFromDB();
        }
        return  restaurantInfos;
    }

    public static List<RestaurantInfo> getAllRestaurants(Integer pageNumber, Integer pageSize)
            throws SQLException {
        List<RestaurantInfo>restaurantInfos = RestaurantMapper.getInstance().getAllRestaurants(pageNumber, pageSize);
        return fillFoodsForRestaurants(restaurantInfos);
    }

    public static List<RestaurantInfo> getSearchedRestaurantsByName(Integer pageNumber, Integer pageSize, String search)
            throws SQLException {
        List<RestaurantInfo>restaurantInfos = RestaurantMapper.getInstance().getSearchedRestaurantsByName(
                pageNumber, pageSize, search);
        return fillFoodsForRestaurants(restaurantInfos);
    }

    public static List<RestaurantInfo> getSearchedRestaurantsByFoodName(Integer pageNumber, Integer pageSize, String search)
            throws SQLException {
        List<RestaurantInfo>restaurantInfos = RestaurantMapper.getInstance().getSearchedRestaurantsByFoodName(
                pageNumber, pageSize, search);
        return fillFoodsForRestaurants(restaurantInfos);
    }

    public static RestaurantInfo getRestaurantWithMenuById(String id) throws SQLException, NotFoundException {
        RestaurantInfo restaurantInfo = RestaurantMapper.getInstance().find(id);
        if(restaurantInfo == null){
            return null;
        }
        restaurantInfo.setMenuFromDB();
        return restaurantInfo;
    }

    public String toJson() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }
}
