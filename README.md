# FoodStore

FoodStore is UT Internet Engineering Course Spring2020 Project. It is a web application for reserving and ordering foods.
This project's backend is developed in Java Spring and frontend is developed with react. Docker-compose is used for deployment.
You can access http://ie.etuts.ir:32222/ if server still up.

![image of first page](./cover.png)
